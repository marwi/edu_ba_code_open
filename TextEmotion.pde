public class TextEmotionData
{
	String text;
	float emotionValue;
	String emotionType;
	color col;
	color textColor;

	public TextEmotionData(String text, float emotionValue, String emotionType)
	{
		this.text = text;
		this.emotionValue = emotionValue;
		this.emotionType = emotionType;

		float alpha = 0;
		if(emotionValue < .5) alpha = 20;
		else if(emotionValue < .75) alpha = 50;
		else if(emotionValue <= 1.0) alpha = 180;


		textColor = color(0);

		switch(emotionType)
		{
			case "anger":
			col = color(200, 40, 0, alpha);
			if(emotionValue >= .75) textColor = color(255);
			break;

			case "fear":	
			col = color(50, 50, 200, alpha);
			if(emotionValue >= .75) textColor = color(255);
			break;

			case "sadness":
			col = color(60, 180, 220, alpha);
			break;

			case "joy":
			col = color(110, 160, 50, alpha);
			break;

			case "disgust": 
			col = color(180, 170, 0, alpha);
			break;

			default:
			col = color(255,10);
			break;
		}
	}


}

public class TextEmotion extends GenHisto<TextEmotionData>
{
	Textbox text;

	public TextEmotion(TextEmotionData data)
	{
		super(data);
	}

	float getWidth()
	{
		return text.getWidth();
	}

	void setup()
	{
		String text = data.text;
		Textbox box = new Textbox(text, Alignment.CENTER, Alignment.CENTER);

		float spacing = 2;
		box.size = (int) (h - spacing*2);
		box.col = data.textColor;
		//box.wrap = false;
		box.setup();
		box.setWidth(box.getTextWidth() + 5);
		box.setX(x);// + box.getWidth()/2);
		box.setY(y-spacing);
		box.setHeight(h);
		this.text = box;

		setWidth(getWidth());
	}

	void draw()
	{
		drawEmotionColor();
		text.draw();
	}

	void drawEmotionColor()
	{
		noStroke();
		fill(data.col);
		rect(x,y,w,h);
	}
}

