import rita.*;

public interface ICombinator <T, U>
{
	T combinate(U input);
}

public class AuthorMaker implements ICombinator<Author, Author[]>
{
	Author combinate(Author[] authors)
	{
		String firstName = null, lastName = null;
		int rounds = 0, maxRounds = 10;

		while(firstName == null && lastName == null && rounds < maxRounds)
		{
			Author rnd = authors[(int)random(0, authors.length)];

			if(firstName == null)
			{
				firstName = firstUp(rnd.firstName.toLowerCase());
			}
			else if(lastName == null)
			{
				lastName = firstUp(rnd.lastName.toLowerCase());
			}
		}

		if(firstName == null || lastName == null)
			return getTotalRandomName(authors);
		else
			return new Author(firstName, lastName);
	}

	Author getTotalRandomName(Author[] authors)
	{
		String first = "", last = "";
		for(Author author : authors)
		{
			if(author == null) continue;
			if(author.hasFullName())
			{
				first += author.firstName;
				last += author.lastName;
			}
		}

		String firstName = makeRandom(first.toLowerCase());
		firstName = firstUp(firstName);

		String lastName = makeRandom(last.toLowerCase());
		lastName = firstUp(lastName);

		println(firstName + " " + lastName);;

		return new Author(firstName, lastName);
	}

	String makeRandom(String input)
	{
		String rnd = "";
		String[] pairs = makePair(input, (int)random(3,6));
		for(int i = 0; i < (int)random(1, 3); i++)
		{
			String p = pairs[(int)random(0, pairs.length)];
			rnd += p;
		}
		return rnd;
	}

	String[] makePair(String input, int count)
	{
		ArrayList<String> full = new ArrayList<String>();
		for(int i = 0; i < input.length(); i += count)
		{
			int end = i+count;
			if(end > input.length() || i+count >= input.length()) 
				end = input.length();

			String str = input.substring(i, end);
			full.add(str);
		}
		return full.toArray(new String[full.size()]);
	}

	String firstUp(String input)
	{
		char first = Character.toUpperCase(input.charAt(0));
		return first + input.substring(1);
	}
}

public class TitleMaker implements ICombinator<String, Strophe[]>
{
	RiMarkov rm;

	public TitleMaker()
	{
		rm = new RiMarkov(2);
	}

	String combinate(Strophe[] strophes)
	{
		String txt = "";
		for(Strophe strophe : strophes)
		{
			for(Verse v : strophe.getVerses()) 
			{
				txt += v.text + ". ";
			}
		}
		
		rm.loadText(txt);

		String title = null;

		do
		{
			title = rm.generateSentence();

		}
		while(title == null);

		title = title.replace(".", "");
		String[] words = RiTa.tokenize(title);

		title = "";
		int start = (int)random(0, words.length/2);
		for(int i = start; i < words.length && i < start + (int)random(5, 7); i++)
			title += words[i] + " ";

		println(title);
		return title;
	}
}