public class CatWordCell extends GenHisto<ArticleStringCollection>
{
	ArrayList<IHisto> histos, left, right;
	IFilter<ArrayList<ArticleStringCollection>, ArrayList<Article>> filterBefore, filterAfter;
	List<ArticleStringCollection> before, after;

	Textbox mainWord;
	float alpha = 0;

	public CatWordCell(ArticleStringCollection col)
	{
		super(col);
	}

	public CatWordCell(ArticleStringCollection col, 
		IFilter<ArrayList<ArticleStringCollection>, ArrayList<Article>> filterBefore,
		IFilter<ArrayList<ArticleStringCollection>, ArrayList<Article>> filterAfter)
	{
		this(col);
		this.filterBefore = filterBefore;
		this.filterAfter = filterAfter;
	}

	void setup()
	{
		histos = new ArrayList<IHisto>();
		left = new ArrayList<IHisto>();
		right = new ArrayList<IHisto>();

		Textbox text = new Textbox(articles.getString());
		text.size = 22;
		text.vertical = Alignment.BOTTOM;
		text.setX(x);
		text.setY(y+18);
		text.setWidth(w);
		text.setHeight(26);
		histos.add(text);
		mainWord = text;

		IHisto dots = new DotDiagram(articles.get());
		dots.setX(x);
		dots.setY(text.getY() + text.getHeight() + 8);
		dots.setWidth(w);
		dots.setHeight(10);
		histos.add(dots);

		DotCircleHisto circle = new DotCircleHisto(articles.get());
		circle.circleSizeMultiplier = 1.2;
		circle.setWidth(110);
		circle.setHeight(110);
		circle.setX(x + w/2);
		circle.setY(dots.getY() + circle.getHeight()/2 + 20);
		histos.add(circle);


		IHisto emotions = new EmotionBars(articles.get());
		emotions.setWidth(75);
		emotions.setHeight(50);
		emotions.setX(x + w/2 - emotions.getWidth()/2);
		emotions.setY(circle.getY() + circle.getHeight()/2 + 15);
		histos.add(emotions);

		textCenterLeft = x + w/2;


		float maxAssoziations = 4;

		float startY = dots.getY() + dots.getHeight() + 12;
		float lineHeight = 27;
		float distanceToCircle = 25;
		float associationWidth = (w - circle.getWidth()) / 2 - distanceToCircle;

		ArrayList<Article> data = articles.get();
		if(filterBefore != null)
		{
			before = filterBefore.filter(data);

			for(int i = 0; i < before.size() && i < maxAssoziations; i++)
			{
				ArticleStringCollection acol = (ArticleStringCollection) before.get(i);
				String str = acol.getString();
				int count = acol.get().size();
				Association ass = new Association(acol, Alignment.RIGHT);
				ass.setX(x);
				ass.setY(startY + lineHeight * i);
				ass.setWidth(associationWidth);
				ass.setHeight(lineHeight);
				ass.setup();
				left.add(ass);
			}
		}

		if(filterAfter != null)
		{
			after = filterAfter.filter(data);

			for(int i = 0; i < after.size() && i < maxAssoziations; i++)
			{
				ArticleStringCollection acol = (ArticleStringCollection) after.get(i);
				String str = acol.getString();
				int count = acol.get().size();
				Association ass = new Association(acol, Alignment.LEFT);
				ass.setX(x + (w - associationWidth));
				ass.setY(startY + lineHeight * i);
				ass.setWidth(associationWidth);
				ass.setHeight(lineHeight);
				ass.setup();
				right.add(ass);
			}
		}
	}

	float textCenterLeft = 0;

	void draw()
	{
		noStroke();
		fill(0, alpha);
		rect(x,y,w,h);


		float curveRoundness = 20;

		int li = 0;
		for(IHisto histo : left)
		{
			histo.draw();

			if(li > 0) continue;
			li++;

			if(articles.size() > 100) continue;

			float lx1 = histo.getX() + histo.getWidth() + 1;
			float ly1 = histo.getY() - 10;
			float lx2 = mainWord.getX() + mainWord.getWidth()/2 - mainWord.getTextWidth()/2 - 30;
			float ly2 = mainWord.getY() + mainWord.getHeight()/2 + 5;

			noFill();
			stroke(0, 50);
			strokeWeight(1);
			//line(lx1, ly1, lx1, ly2);
			//line(lx1, ly2, lx2, ly2);
			//bezier(lx1, ly1, lx1, ly1 - 10, lx2 - 10, ly2, lx2, ly2);


			float sx = histo.getX() + histo.getWidth() + 10;
			float sy = histo.getY() + histo.getHeight()/2;

			float ex = (mainWord.getX() + mainWord.getWidth()/2) - mainWord.getTextWidth()/2 - 20;
			float ey = mainWord.getY() + mainWord.getHeight()/2 + 2;

			noFill();
			stroke(0, 20);
			strokeWeight(1);
			//bezier(sx, sy, sx+curveRoundness, sy + 5, ex-curveRoundness*1.5, ey, ex, ey);
		}

		int ri = 0;
		for(IHisto histo : right)
		{
			histo.draw();

			if(ri > 0) continue;
			ri++;

			if(articles.size() > 80) continue;

			float lx1 = histo.getX() - 1;
			float ly1 = histo.getY() - 10;
			float lx2 = mainWord.getX() + mainWord.getWidth()/2 + mainWord.getTextWidth()/2 + 30;
			float ly2 = mainWord.getY() + mainWord.getHeight()/2 + 5;

			noFill();
			stroke(0, 50);
			strokeWeight(1);
			//bezier(lx1, ly1, lx1, ly1-10, lx2+10, ly2, lx2, ly2);
			//line(lx1, ly1, lx1, ly2);
			//line(lx1, ly2, lx2, ly2);

			float ex = histo.getX() - 10;
			float ey = histo.getY() + histo.getHeight()/2;

			float sx = (mainWord.getX() + mainWord.getWidth()/2) + mainWord.getTextWidth()/2 + 20;
			float sy = mainWord.getY() + mainWord.getHeight()/2 + 2;

			noFill();
			stroke(0, 20);
			strokeWeight(1);
			//bezier(sx, sy, sx+curveRoundness*1.5, sy, ex-curveRoundness, ey + 5, ex, ey);
		}

		for(IHisto histo : histos)
			histo.draw();
	}
}