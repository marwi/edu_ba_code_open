public class ThemeCollector
{
	ArrayList<Article> articles;
	public ThemeCollector(ArrayList<Article> articles)
	{	
		this.articles = articles;
	}

	ArrayList<Sentence> allSentences;
	ArrayList<Sentence> getAllSentences()
	{
		if(allSentences != null)
		{
			return allSentences;
		}
		else
		{
			return getAllSentences(articles);
		}
	}

	ArrayList<Sentence> getAllSentences(ArrayList<Article> articles)
	{
		ArrayList<Sentence> allSentences = new ArrayList<Sentence>();

		println("articles : " + articles.size());

		for(Article a : articles)
		{
			allSentences.addAll(a.sentences);
		}

		return allSentences;
	}

	ArrayList<Sentence> getForRandomCategory()
	{
		ArrayList<Sentence> sentences = new ArrayList<Sentence>();

		String[] cats = new String[]{"europe", "asia", "world", "arts", "sports", "americas", "middle east"};
		String cat = cats[(int)random(0,cats.length-1)];

		IFilter filter = new CategoryFilter(cat);
		ArrayList<Article> filtered = (ArrayList<Article>)filter.filter(articles);

		sentences = getAllSentences(filtered);

		return sentences;
	}

	ArrayList<Sentence> getSentencesAbout(String[] words)
	{
		ArrayList<Sentence> sentences = new ArrayList<Sentence>();

		for(Article a : articles)
		{
			if(a.sentences == null) continue;
			for(Sentence sentence : a.sentences)
			{
				boolean found = false;
				if(found) continue;
				for(String word : words)
				{
					if(sentence.text.toLowerCase().contains(word.toLowerCase()))
					{
						sentences.add(sentence);
						found = true;
					}
				}
			}
		}

		println("found " + sentences.size() + " sentences");

		return sentences;
	}
}