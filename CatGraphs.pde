
// graphs for one category:

public class CatGraph extends Histo
{
	ArrayList<IHisto> histos;
	String category;
	float textY;

	public CatGraph(ArrayList<Article> articles, String category)
	{
		super(articles);
		this.category = category;
	}

	void setup()
	{
		histos = new ArrayList<IHisto>();

		IFilter filter = new CategoryFilter(category);
		articles = (ArrayList<Article>)filter.filter(articles);

		float spacingTop = 80;
		float spacingBottom = 60;
		float innerHeight = h - (spacingTop + spacingBottom);
		float graphHeight = innerHeight / 3.12;
		float graphWidth = w*.49;

		float graphX = x;
		float graphY = y + spacingTop;

		CatWords words = new CatWords(articles);
		words.setX(graphX);
		words.setY(graphY);
		words.setWidth(w);
		words.setHeight(innerHeight);
		words.setup();
		histos.add(words);

		textY = y + 45;
	}

	void draw()
	{
		fill(0);
		textSize(40);
		textAlign(CENTER, BOTTOM);
		text(category, x + w/2, textY);

		float textWidth = textWidth(category);
		textSize(14);
		text(articles.size(), x+w/2 + textWidth/2 + 40, textY-2);

		noFill();
		strokeWeight(1);
		stroke(0, 50);
		//line(x, y, x+w, y);

		for(IHisto histo : histos)
			histo.draw();
	}
}