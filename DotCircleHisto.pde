public class DotCircleHisto extends Histo
{
	int maxColors = 12;
	float circleSizeMultiplier = 1;
	float alphaMultiplier = 1;
	float percentageMultiplier = 1;
	color background = color(255,50);
	boolean considerSaturation = false;

	public DotCircleHisto(Article article)
	{
		super(new ArrayList<Article>());
		articles.add(article);
	}

	public DotCircleHisto(ArrayList<Article> articles)
	{
		super(articles);
		setup();
	}

	void setup()
	{
	}

	void draw()
	{
		float dia = w;

		noStroke();
		fill(background);
		ellipse(x, y, dia, dia);

		/*for(int i = 0; i < dia; i++)
		{
			float ndia = i / dia * dia;
			float range = 15;
			float brightness = range - i/dia * range + (255 - range);
			noFill();
			stroke(brightness, 50);
			strokeWeight(3);
			ellipse(x,y,ndia,ndia);
		}*/

		noFill();
		strokeWeight(1);
		stroke(0, 40);
		ellipse(x, y, dia, dia);

		//blendMode(LIGHTEST);

		for(Article a : articles)
		{
			for(int i = 0; i < a.paletteSize() && i < maxColors; i++)
			{
				color col = a.getColor(i);
				float perc = a.getColorPercent(i);

				PVector vec = new PVector(1,0);

				float hue = hue(col);
				float angle = hue/(float)255 * 360;
				float brightness = brightness(col);
				float saturation = saturation(col);
				float length = brightness/255 * dia/2;

				if(considerSaturation)
					length = length/2 + saturation/255 * dia/4;

				vec.mult(length);
				vec.rotate(radians(angle));

				float dotRadius = 4 + brightness(col)/255 * 4;
				dotRadius += perc * 100 * percentageMultiplier;
				dotRadius *= circleSizeMultiplier;

				float alpha = 120;
				alpha += perc * 400 * percentageMultiplier;
				alpha *= alphaMultiplier;

				//dotRadius -= 60/(saturation(col)+1) * 2;

				//if(dotRadius < 5) dotRadius = 5;

				if(brightness > 240)
				{
					strokeWeight(.2);
					stroke(0, 30);
				}
				else 
				{
					noStroke();	
				}

				fill(col, alpha);
				ellipse(x+vec.x, y+vec.y, dotRadius, dotRadius);
			}	
		}

		//blendMode(BLEND);
	}
}
