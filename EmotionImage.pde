public class EmotionImage extends Histo
{
	public Date earliest, oldest;

	ArrayList<Article> dataSet;
	int year;
	String emotion;
	float emotionMinimum;

	ArrayList<Sentence> sentences;

	public EmotionImage(ArrayList<Article> articles, String emotion) 
	{ 
		super(articles);
		this.emotion = emotion;
		setup();
	}

	void setup()
	{
		emotionMinimum = .2;
		year = 2013;
		dataSet = new ArrayList<Article>();
		sentences = new ArrayList<Sentence>();

		for(Article a : articles)
		{
			Date d = a.pubDate;
			int y = d.getYear();
			if(y == year && a.documentType.contains("article"))
			{
				addArticle(a);
			}
		}

		myFont = createFont("Palatino Linotype", 30);
	}

	void addArticle(Article a)
	{
		if(a.getEmotion(emotion) > emotionMinimum)
			dataSet.add(a);

		for(int i = 0; i < a.sentences.size(); i++)
		{
			Sentence s = a.sentences.get(i);
			float em = s.getEmotion(emotion);
			if(em >= emotionMinimum)
			{
				sentences.add(s);
			}
		}
	}


	PFont myFont;

	void draw()
	{
		noFill();
		stroke(0, 0);
		rect(x,y,w,h);


		Collections.sort(this.sentences, new Comparator<Sentence>(){
			public int compare(Sentence a1, Sentence a2) 
			{
				return a1.article.pubDate.totalDays() - a2.article.pubDate.totalDays(); 				
			}
		});

		Collections.sort(this.dataSet, new Comparator<Article>(){
			public int compare(Article a1, Article a2) 
			{
				float emotion1 = a1.getEmotion(emotion);
				float emotion2 = a2.getEmotion(emotion); 	
				if(emotion1 > emotion2) return -1;
				else if(emotion2 > emotion1) return 1;
				else return 0;			
			}
		});

		float px = x;
		float py = y + 150;

		float columnWidth = w/4;

		for(Article article : dataSet)
		{
			String t = article.title + " (" + article.pubDate + ")\n";
			Date date = article.pubDate;
			int days = date.totalDays();
			// spalten zu 4 jahreszeiten
			int columnID = (int)(((float)days/(float)365)*4);
			println(days + " - " + columnID);

			for(int i = 0; i < 4; i++)
			{
				noFill();
				strokeWeight(2);
				stroke(200);
				rect(x + i*columnWidth, py, columnWidth, columnWidth);
			}

			float columnX = x + columnID * columnWidth;

			PImage img = article.getImage();
			if(img != null)
			{
				float imgSize = 100;
				image(img, columnX + columnWidth/2 - imgSize/2, py + columnWidth/2 - imgSize/2, imgSize, imgSize);
			}

			fill(0);  
			textFont(myFont);
			textAlign(LEFT, BOTTOM);
			textSize(11);
			textLeading(20);
			text(date + " - " + article.getEmotion(emotion), columnX, py + columnWidth);

			py += columnWidth*1.1;
		}


		String description =  "emotion: \"" + emotion + "\", minimum: " + emotionMinimum;
		description += "\n" + dataSet.size() + " titles, sorted by emotion up (high) to down (low), " + year;

		fill(0);
		textAlign(LEFT, TOP);
		textSize(20);
		text(description, x + 5, y);
	}
}