public interface IFilter<T, U>
{
	T filter(U el);
}

public enum WordPosition { ANY, BEFORE, AFTER };


public class WordFilter implements IFilter<ArrayList<ArticleStringCollection>, ArrayList<Article>>
{
	RiLexicon lexicon;
	String wordType;

	public WordFilter(String wordType)
	{
		lexicon = new RiLexicon();
		this.wordType = wordType;
	}

	public ArrayList<ArticleStringCollection> filter(ArrayList<Article> articles)
	{
		return getCollection(articles, wordType);
	}

	ArrayList<ArticleStringCollection> getCollection(ArrayList<Article> articles, String wordType)
	{
		HashMap<String, ArticleStringCollection> words = new HashMap<String, ArticleStringCollection>();

		for(Article a : articles)
		{
			IFilter filter = new TitleWords();
			String[] titleWords = (String[])filter.filter(a);

			IFilter filter2 = new ParagraphWords();
			//String[] paragraphWords = (String[])filter2.filter(a);

			String[] singleWords = titleWords; //concat(titleWords, paragraphWords);

			/*WordFilter filter3 = new TextWords();
			String[] textWords = filter3.get(a);
			singleWords = concat(singleWords, textWords);*/

			for(String str : singleWords)
			{
				if(!isType(str, wordType))
					continue;

				if(words.get(str) == null)
				{
					words.put(str, new ArticleStringCollection(a, str));
				}
				else 
				{
					ArticleStringCollection col = words.get(str);
					col.add(a);	
					words.put(str, col);
				}
			}
		}

		ArrayList<ArticleStringCollection> wordCollection = new ArrayList<ArticleStringCollection>(words.values());

		// sort by appearance
		Collections.sort(wordCollection, new Comparator<ArticleStringCollection>()
		{
			public int compare(ArticleStringCollection col1, ArticleStringCollection col2)
			{
				int s1 = col1.size();
				int s2 = col2.size();
				if(s1 > s2) return -1;
				else if(s1 < s2) return 1;
				else return 0;
			}
		});

		return wordCollection;
	}

	boolean isType(String word, String wordType)
	{
		switch(wordType)
		{
			case "adverb":
			if(lexicon.isAdverb(word)) return true;
			break;

			case "adjective":
			if(lexicon.isAdjective(word) 
				&& lexicon.isAdverb(word) == false
				&& lexicon.isVerb(word) == false
				&& lexicon.isNoun(word) == false) 
			{
				return true;
			}
			break;

			case "noun":
			if(lexicon.isNoun(word) 
				&& lexicon.isAdjective(word) == false 
				&& lexicon.isAdverb(word) == false
				&& lexicon.isVerb(word) == false) 
				return true;
			break;

			case "verb":
			if(lexicon.isVerb(word)
				&& lexicon.isNoun(word) == false
				&& lexicon.isAdverb(word) == false
				&& lexicon.isAdjective(word) == false
				&& word.toLowerCase() != "he")
				return true;
			break;


			default:
			return false;
		}

		return false;
	}
}

public class RelevantWordFilter implements IFilter<ArrayList<ArticleStringCollection>, ArrayList<Article>>
{

	RiLexicon lexicon;
	String keyWord;
	String[] wordTypes;
	WordPosition wordPosition;
	// how many words before or after to check
	int maxSearchLength;

	public RelevantWordFilter(String keyWord, String[] wordTypes, WordPosition wordPosition)
	{
		this(keyWord, wordTypes, wordPosition, 999);
	}

	public RelevantWordFilter(String keyWord, String[] wordTypes, WordPosition wordPosition, int maxSearchLength)
	{
		this.keyWord = keyWord;
		this.wordTypes = wordTypes;
		this.wordPosition = wordPosition;
		this.maxSearchLength = maxSearchLength;
		lexicon = new RiLexicon();
	}

	public ArrayList<ArticleStringCollection> filter(ArrayList<Article> articles)
	{
		return getCollection(articles);
	}

	ArrayList<ArticleStringCollection> getCollection(ArrayList<Article> articles)
	{
		HashMap<String, ArticleStringCollection> words = new HashMap<String, ArticleStringCollection>();

		for(Article a : articles)
		{
			IFilter filter = new TitleWords();
			String[] titleWords = (String[])filter.filter(a);

			String[] singleWords = titleWords; 

			boolean keyWordFound = false;

			for(int i = 0; i < singleWords.length; i++)
			{
				String str = singleWords[i];
				RiString ristr = new RiString(str);

				if(keyWordFound == false && ristr.equalsIgnoreCase(keyWord))
				{
					keyWordFound = true;
				}
				else if(keyWordFound)
				{
					keyWordFound = false;

					// check words before / current words
					switch(wordPosition)
					{
						case BEFORE:
						int wi = i-1;
						int wordsChecked = 0;
						boolean foundType = false;
						// check a number of words before
						while(wi > 0 && !foundType && wordsChecked < maxSearchLength)
						{
							wi = wi - 1;
							str = singleWords[wi];

							for(String type : wordTypes)
							{
								if(isType(str, type))
									foundType = true;
							}

							wordsChecked += 1;
						}
						if(foundType == false) 
							continue;
						break;

						case AFTER:
						int wiAfter = i;
						int wordsCheckedAfter = 0;
						boolean foundTypeAfter = false;
						// check a number of words before
						while(wiAfter < singleWords.length 
							&& !foundTypeAfter 
							&& wordsCheckedAfter < maxSearchLength)
						{
							str = singleWords[wiAfter];
							wiAfter += 1;

							for(String type : wordTypes)
							{
								if(isType(str, type))
									foundTypeAfter = true;
							}

							wordsCheckedAfter += 1;
						}

						if(!foundTypeAfter) continue;
						break;
					}

					if(words.get(str) == null)
					{
						words.put(str, new ArticleStringCollection(a, str));
					}
					else 
					{
						ArticleStringCollection col = words.get(str);
						col.add(a);	
						words.put(str, col);
					}
				}
			}
		}

		ArrayList<ArticleStringCollection> wordCollection = new ArrayList<ArticleStringCollection>(words.values());

		// sort by appearance
		Collections.sort(wordCollection, new Comparator<ArticleStringCollection>()
		{
			public int compare(ArticleStringCollection col1, ArticleStringCollection col2)
			{
				int s1 = col1.size();
				int s2 = col2.size();
				if(s1 > s2) return -1;
				else if(s1 < s2) return 1;
				else return 0;
			}
		});

		return wordCollection;
	}

	boolean isType(String word, String wordType)
	{
		switch(wordType)
		{
			case "adverb":
			if(lexicon.isAdverb(word)) return true;
			break;

			case "adjective":
			if(lexicon.isAdjective(word) 
				&& lexicon.isAdverb(word) == false
				&& lexicon.isVerb(word) == false
				&& lexicon.isNoun(word) == false) 
			{
				return true;
			}
			break;

			case "noun":
			if(lexicon.isNoun(word) 
				&& lexicon.isAdjective(word) == false 
				&& lexicon.isAdverb(word) == false
				&& lexicon.isVerb(word) == false) 
				return true;
			break;

			case "verb":
			if(lexicon.isVerb(word)
				&& lexicon.isNoun(word) == false
				&& lexicon.isAdverb(word) == false
				&& lexicon.isAdjective(word) == false
				&& word.toLowerCase() != "he")
				return true;
			break;


			default:
			return false;
		}

		return false;
	}
}

public class CategoryFilter implements IFilter<ArrayList<Article>, ArrayList<Article>>
{
	String category;

	public CategoryFilter(String category)
	{
		this.category = category;
	}

	ArrayList<Article> filter(ArrayList<Article> articles)
	{
		ArrayList<Article> filtered = new ArrayList<Article>();

		for(Article a : articles)
		{
			if(a.subSection == null) 
				continue;

			boolean select = a.section.toLowerCase().contains(category) 
			|| a.subSection.toLowerCase().contains(category);

			if(select)
				filtered.add(a);
		}

		return filtered;
	}
}

public class YearFilter implements IFilter<ArrayList<Article>, ArrayList<Article>>
{
	int year;

	public YearFilter(int year)
	{
		this.year = year;
	}

	ArrayList<Article> filter(ArrayList<Article> articles)
	{
		ArrayList<Article> filtered = new ArrayList<Article>();

		for(Article a : articles)
		{
			Date d = a.pubDate;
			int y = d.getYear();

			if(y == year)
			{
				filtered.add(a);
			}
		}

		return filtered;
	}
}

public class MonthFilter implements IFilter<ArrayList<Article>, ArrayList<Article>>
{
	int month;
	public MonthFilter(int month)
	{
		this.month = month;
	}

	ArrayList<Article> filter(ArrayList<Article> articles)
	{
		ArrayList<Article> filtered = new ArrayList<Article>();

		for(Article a : articles)
		{
			Date d = a.pubDate;
			int month = d.getMonth();

			if(month == this.month)
				filtered.add(a);
		}

		return filtered;
	}
}

// filter by day of year
public class DayFilter implements IFilter<ArrayList<Article>, ArrayList<Article>>
{
	int day;
	int endDay = 0;

	public DayFilter(int day)
	{
		this.day = day;
	}

	public DayFilter(int day, int extension)
	{
		this(day);
		this.endDay = day + extension;
	}

	ArrayList<Article> filter(ArrayList<Article> articles)
	{
		ArrayList<Article> filtered = new ArrayList<Article>();

		for(Article a : articles)
		{
			Date d = a.pubDate;
			int cur = d.totalDays();

			if(cur == this.day || cur >= this.day && cur <= endDay)
			{
				filtered.add(a);
			}
		}

		return filtered;
	}
}

public class StringFilter implements IFilter<ArrayList<Article>, ArrayList<Article>>
{
	String[] strings;
	boolean inTitle = true;
	boolean inParagraph = false;
	boolean inText = false;

	public StringFilter(String string)
	{
		string = string.toLowerCase();
		this.strings = new String[]{string};
	}

	public StringFilter(String[] strings)
	{
		for(int i = 0; i < strings.length; i++)
		{
			strings[i] = strings[i].toLowerCase();
		}

		this.strings = strings;
	}

	ArrayList<Article> filter(ArrayList<Article> articles)
	{
		ArrayList<Article> filtered = new ArrayList<Article>();

		for(Article a : articles)
		{
			if(hasString(a))
			{
				filtered.add(a);
			}
		}

		return filtered;
	}

	private boolean hasString(Article a)
	{		
		for(String str : strings)
		{
			if(inTitle)
			{
				if(a.title.toLowerCase().contains(str))
					return true;
			}

			if(inParagraph)
			{
				if(a.leadParagraph.toLowerCase().contains(str))
					return true;
			}

			if(inText)
			{
				if(a.text.toLowerCase().contains(str))
					return true;
			}
		}

		return false;
	}
}

public class SentenceStringFilter implements IFilter<ArrayList<Sentence>, ArrayList<Sentence>>
{
	String filter;
	public SentenceStringFilter(String filter) { this.filter = filter; }
	public ArrayList<Sentence> filter(ArrayList<Sentence> sentences)
	{
		ArrayList<Sentence> filtered = new ArrayList<Sentence>();

		filter = filter.toLowerCase();
		for(Sentence sentence : sentences)
		{
			if(sentence.text.toLowerCase().contains(filter))
			{
				filtered.add(sentence);
			}
		}
		return filtered;
	}
}

public class TitleWords implements IFilter<String[], Article>
{
	String[] filter(Article a)
	{
		String text = a.title;
		text = text.toLowerCase();
		//String[] words = splitTokens(title, " :,()");
		return RiTa.tokenize(text, "\\s");
	}
}

public class ParagraphWords implements IFilter<String[], Article>
{
	String[] filter(Article a)
	{
		String text = a.leadParagraph;
		text = text.toLowerCase();
		return RiTa.tokenize(text, "\\s");
	}
}

public class TextWords implements IFilter<String[], Article>
{
	String[] filter(Article a)
	{
		String text = a.text;
		text = text.toLowerCase();
		return RiTa.tokenize(text, "\\s");
	}
}

public class EmotionFilter implements IFilter<ArrayList<Article>, ArrayList<Article>>
{
	// filter highest emotion

	String type;

	public EmotionFilter(String type)
	{
		this.type = type;
	}

	ArrayList<Article> filter(ArrayList<Article> articles)
	{
		ArrayList<Article> filtered = new ArrayList<Article>();

		float currentlyHighest = 0;

		for(Article a : articles)
		{
			float value = a.getEmotion(type);

			if(value > currentlyHighest)
			{
				filtered.clear();
				filtered.add(a);
				currentlyHighest = value;
			}
			else if(value == currentlyHighest)
			{
				filtered.add(a);
			}
		}

		return filtered;
	}
}

public class ArticleFilter implements IFilter<ArrayList<Article>, ArrayList<Article>>
{
	public ArrayList<Article> filter(ArrayList<Article> articles)
	{
		ArrayList<Article> filtered = new ArrayList<Article>();
		for(Article a : articles)
		{
			if(a.documentType.toLowerCase().contains("article"))
				filtered.add(a);
		}
		return filtered;
	}
}

public class WordCountFilter implements IFilter<ArrayList<Article>, ArrayList<Article>>
{
	int min, max;

	public WordCountFilter(int min, int max)
	{
		this.min = min;
		this.max = max;
	}

	public ArrayList<Article> filter(ArrayList<Article> articles)
	{
		ArrayList<Article> filtered = new ArrayList<Article>();
		for(Article a : articles)
		{
			if(a.wordCount >= min && a.wordCount <= max)
			{
				filtered.add(a);
			}
		}
		return filtered;
	}
}

