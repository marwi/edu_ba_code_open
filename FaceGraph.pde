import gab.opencv.*;
import java.awt.Rectangle;

public class FaceGraph extends Histo
{
	ArrayList<IHisto> faces;

	public FaceGraph(ArrayList<Article> articles, float x, float y, float w, float h)
	{
		super(articles, x, y, w, h);
	}

	void setup()
	{
		faces = new ArrayList<IHisto>();

		boolean checkFaces = false;

		for(Article a : articles)
		{
			String img = a.getImagePath();

			if(!checkFaces)
			{
				addImage(a);
			}
			else  
			{
				OpenCV opencv = new OpenCV(applet, img);
				opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
				opencv.loadImage(img);
				Rectangle[] _faces = opencv.detect();

				if(_faces.length > 0)
				{
					addImage(a);
				}
			}

			//if(faces.size() > 2000) break;
		}


		LeftAligned layouter = new LeftAligned(3,3);
		layouter.layout(faces, x, y, w, h);
	}

	void addImage(Article a)
	{
		ImageBox imgBox = new ImageBox(a.getImage(), -100, -100, w/150, w/150);
		faces.add(imgBox);
	}

	void draw()
	{
		for(IHisto histo : faces)
		{
			histo.draw();
		}
	}
}
