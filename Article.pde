// D:\Personal\Uni\2016\UrbaneEbenen\nyt_downloads\4fd2a5688eb7c8105d88cc28

public class Article extends ToneData
{
	boolean valid = false;
	boolean isValid() { return valid; }

	String path;
	String url, id, text, title, leadParagraph;
	String documentType;
	String section, subSection;
	Date pubDate;
	Author[] authors;

	int wordCount;

	int paletteSize, imageWidth, imageHeight;
	ArrayList<ColorInfo> colors;
	ArrayList<Sentence> sentences;

	Article(String path)
	{
		try 
		{
			this.path = path;
			JSONObject json = loadJSONObject(path + "/article.json");
			JSONObject palette = loadJSONObject(path + "/palette.json");

			if(palette.size() > 0)
			{
				Init(json, palette);
			}
		}
		catch(Exception e) 
		{ 
			//println("Article: " + e); 
			valid = false; 
		}
	}

	private void Init(JSONObject json, JSONObject paletteJson)
	{
		// ARTICLE

		JSONObject article = json.getJSONObject("article");
		url = article.getString("web_url");
		id = article.getString("_id");
		pubDate = new Date(article.getString("pub_date"));
		text = json.getString("text");
		title = article.getJSONObject("headline").getString("main");
		documentType = article.getString("document_type");
		leadParagraph = article.getString("lead_paragraph");

		try { section = article.getString("section_name"); }
		catch(Exception e) {  }
		
		try { subSection = article.getString("subsection_name");  }
		catch(Exception e) {  }

		try { wordCount = parseInt(article.getString("word_count")); }
		catch(Exception e) { wordCount = getWords().length; }

		try
		{
			JSONArray authorsArray = article.getJSONObject("byline").getJSONArray("person");
			authors = new Author[authorsArray.size()];
			for(int i = 0; i < authorsArray.size(); i++)
			{
				JSONObject a = authorsArray.getJSONObject(i);
				String firstName = a.getString("firstname");
				String lastName = a.getString("lastname");
				Author author = new Author(firstName, lastName);
				authors[i] = author;
			}
		}
		catch(Exception e)
		{
			println(e);
		}


		// TONE

		JSONObject textTone = json.getJSONObject("textTone");
		JSONArray toneCategories = textTone.getJSONArray("tone_categories");
		readTone(toneCategories);


		// COLOR

		JSONArray palette = paletteJson.getJSONArray("palette");
		paletteSize = palette.size();
		imageWidth = paletteJson.getInt("width");
		imageHeight = paletteJson.getInt("height");

		colors = new ArrayList<ColorInfo>();
		colorMode(HSB, 255);
		for(int i = 0; i < paletteSize; i++)
		{
			JSONObject co = palette.getJSONObject(i);
			int h = co.getInt("hue");
			int s = co.getInt("saturation");
			int b = co.getInt("brightness");
			int x = co.getInt("x");
			int y = co.getInt("y");
			int count = co.getInt("count");

			color col = color(h,s,b);
			PVector pos = new PVector(x,y);
			ColorInfo info = new ColorInfo(col, pos);
			info.count = count;
			colors.add(info);
		}
		colorMode(RGB, 255);

		// SENTENCES

		sentences = new ArrayList<Sentence>();
		JSONArray sentencesTone = json.getJSONArray("sentencesTone");

		for(int j = 0; j < sentencesTone.size(); j++)
		{
			JSONObject sentenceObject = sentencesTone.getJSONObject(j);
			Sentence sentence = new Sentence(this, sentenceObject);

			if(sentence.isValid())
			{
				sentences.add(sentence);
			}
		}


		valid = true;
	}

	String getImagePath()
	{
		return path + "/image.jpg";
	}

	PImage getImage()
	{
		return loadImage(getImagePath());
	}

	String[] getWords()
	{
		return splitTokens(text, " ");
	}

	int getWordCount() 
	{
		return wordCount;
	}

	Date getDate()
	{
		return pubDate;
	}

	int paletteSize() { return paletteSize; }
	color getColor(int index)
	{
		try 
		{
			ColorInfo col = colors.get(index);
			return col.get();
		}
		catch(Exception e) 
		{
			println(e);
			return -1;
		}
	}
	int getColorCount(int index)
	{
		if(index >= paletteSize())
		{
			return -1;
		}
		else
		{
			ColorInfo col = colors.get(index);
			return col.count;
		}
	}

	float getColorPercent(int index)
	{
		if(index >= paletteSize())
		{
			return 0;
		}
		else
		{
			ColorInfo col = colors.get(index);
			float count = (float)col.count;
			int w = imageWidth;
			int h = imageHeight;
			float sum = w * h;
			float perc = count / sum;
			return perc;
		}
	}

	Author[] getAuthors()
	{
		return authors;
	}
}

