public class ColorColumn extends Histo
{
	ArrayList<ColorCell> cells;

	public ColorColumn(ArrayList<Article> articles)
	{
		super(articles);
	}

	void setup()
	{
		cells = new ArrayList<ColorCell>();

		float cellCount = count();
		float cellHeight = w;

		for(int i = 0; i < cellCount; i++)
		{
			ColorCell cell = new ColorCell(x, y + cellHeight * i, w, cellHeight);
			cells.add(cell);
		}

		int maxColors = 40;

		for(Article a : articles)
		{
			for(int j = 0; j < a.paletteSize() && j < maxColors; j++)
			{
				color c = a.getColor(j);
				float y = this.y + (float)hue(c) / (float)255 * h;
				boolean found = false;

				for(ColorCell cell : cells)
				{
					if(y >= cell.y && y < cell.y+cell.h)
					{
						cell.add(c);
						break;
					}
				}
			}
		}
	}

	void draw()
	{
		noFill();
		stroke(0,30);
		strokeWeight(1);
		rect(x,y,w,h);

		for(ColorCell cell : cells)
			cell.draw();
	}

	int count()
	{
		return (int)(h / w);
	}

	int size()
	{
		return cells.size();
	}

	ArrayList<Integer> getAverage()
	{
		ArrayList<Integer> avg = new ArrayList<Integer>();
		for(ColorCell cell : cells)
		{
			if(cell.colors.size() > 0)
			{
				color col = cell.getAverage();
				avg.add(col);
			}
			else  
			{
				avg.add(-1);	
			}
		}
		return avg;
	}

	ArrayList<ColorCell> get()
	{
		return cells;
	}

	ColorCell get(int i)
	{
		return (ColorCell) cells.get(i);
	}
}

public class ColorCell
{
	ArrayList<Integer> colors;

	float x, y, w, h;

	// wird in colorgrid berechnet und gesetzt
	float avgHue, avgBright, avgSat;

	public ColorCell(float x, float y, float w, float h)
	{
		this.colors = new ArrayList<Integer>();
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}

	void add(color c)
	{
		colors.add(c);
	}

	void draw()
	{
		if(colors.size() > 0)
		{
			int spacing = 1;

			float innerWidth = w - spacing * 2;
			float innerHeight = h - spacing * 2;

			noFill();
			stroke(0, 50);
			strokeWeight(1);
			rect(x + spacing, y + spacing, w - spacing*2, h - spacing*2);


			float cx = x+w/2;
			float cy = y+w/2;


			color c = getAverage();

			float hue = hue(c);
			float sat = saturation(c);
			float bright = brightness(c);

			float satDiffNorm = (sat - avgSat) / 255;
			float brightDiffNorm = (bright - avgBright) / 255;

			if(abs(satDiffNorm) <= .1 && abs(brightDiffNorm) <= .1)
			{
				fill(0);
				noStroke();
				ellipse(cx, cy, 2, 2);
			}
			else 
			{
					/*
				noStroke();
				fill(c);
				ellipse(x+w/2, y+h/2, 4, 4);*/

				float satx = cx + satDiffNorm * w * .5;
				float bright_y = cy - brightDiffNorm * h * .5;

				stroke(0);

				if(abs(satDiffNorm) > .1)
				{
					strokeWeight(1);
					line(cx, cy, satx, cy);
					strokeWeight(2);
					line(satx, cy - 2, satx, cy+2);
				}
				
				if(abs(brightDiffNorm) > .1)
				{
					strokeWeight(1);
					line(cx, cy, cx, bright_y);
					strokeWeight(2);
					line(cx - 2, bright_y, cx + 2, bright_y);
				}
			}


		}
		else 
		{
			noFill();
			stroke(0, 100);
			strokeWeight(1);
			ellipse(x + w/2, y + h/2, 4, 4);
		}
	}

	color getAverage()
	{
		float totalHue = 0, totalBrightness = 0, totalSaturation = 0;

		for(color c : colors)
		{
			totalHue += hue(c);
			totalBrightness += brightness(c);
			totalSaturation += saturation(c);
		}

		float 
		hue = totalHue/colors.size(),
		brightness = totalBrightness / colors.size(),
		saturation = totalSaturation / colors.size();

		colorMode(HSB, 255);
		color c = color(hue, saturation, brightness);
		colorMode(RGB, 255);
		return c;
	}
}