public class ColorHisto extends Histo
{
	public Date earliest, oldest;

	int colorCount;

	public ColorHisto(ArrayList<Article> articles) 
	{ 
		super(articles);
		setup();
	}

	void setup()
	{
		colorCount = 200;
	}

	void draw()
	{
		noFill();
		strokeWeight(1);
		stroke(0, 50);
		rect(x,y,w,h);

		int total = 0;
		for(int m = 0; m < months.length; m++)
		{
			int month = months[m];

			for(int d = 1; d <= month; d++)
			{
				int sum = d + total;
				float px = x + (float)sum/(float)365 * w;
				strokeWeight(1);

				if(d == month)
				{
					strokeWeight(2);
					stroke(0);
					strokeCap(SQUARE);
					//line(px, y+h+1, px, y+h+3);
					strokeCap(ROUND);

					strokeWeight(4);
					stroke(0, 20);

					if(m+2 <= 12)
					{
						fill(0,100);
						textSize(14);
						textAlign(CENTER, BOTTOM);
						//text(m+2, px, y - 8);
						textAlign(CENTER, TOP);
						//text(m+2, px, y+h+6);
					}
					
				}
				else
					stroke(0, 50);

				line(px, y+1, px, y+h-1);
			}

			total += month;
		}

		for(Article article : articles)
		{
			String t = article.title + " (" + article.pubDate + ")\n";
			Date date = article.pubDate;
			int days = date.totalDays();

			float px = x + (float)days/(float)365 * w;

			for(int i = 0; i < article.paletteSize(); i++)
			{
				color col = article.getColor(i);

				if(col == -1) continue;

				float value = hue(col);

				float py = y + (float)value / (float)255 * h;

				float perc = article.getColorPercent(i);
				float alpha = 10 + perc * 700;

				if(alpha > 200) alpha = 200;

				stroke(0, alpha*.5);
				strokeWeight(.2);
				fill(col, alpha);

				float size = 7 + article.getColorPercent(i) * 80; // 8
				if(size > 30) size = 30;

				float brightness = brightness(col);
				brightness /= 255;
				brightness -= .5;
				brightness *= 2;
				py += brightness * 5;

				ellipse(px, py, size, size);
				//rect(px-size/2, py-size/2, size, size);
			}

			//drawImageAndText(article, px);
			//if(px > lastPx + 230)
			//	drawImageAndText(article, px);
		}
	}

	int lastPx = 0;

	void drawImageAndText(Article article, float px)
	{
		lastPx = (int)px;

		Date date = article.pubDate;

		strokeWeight(1);
		stroke(200);
		line(px, y+h+1, px, y+h+20);

		float imgWidth = 150;
		PImage img = article.getImage();
		img.resize((int)imgWidth, 0);
		image(img, px - imgWidth/2, y + h + 20);

		String title = article.title;
		String leadParagraph = article.leadParagraph;
		textAlign(LEFT, TOP);
		fill(0);
		textSize(14);
		textLeading(20);
		float tx = px-imgWidth/2;
		float ty = y+h+img.height + 30;
		text(title, tx, ty, imgWidth + 70, height);
		textSize(12);
		text(date.toString(), tx, ty + 66);
		text(leadParagraph, tx, ty + 90, imgWidth +60, height);
	}
}