public class ColorGrid extends Histo
{
	ArrayList<ColorColumn> daily;

	public ColorGrid(ArrayList<Article> articles)
	{
		super(articles);
	}

	void setup()
	{
		daily = new ArrayList<ColorColumn>();

		int range = 7;

		for(int i = 0; i + range < 365; i+=range)
		{
			int curDay = i + 1;

			IFilter day = new DayFilter(curDay, range-1);
			ArrayList<Article> filtered = (ArrayList<Article>)day.filter(articles);

			ColorColumn column = new ColorColumn(filtered);

			float xpos = x + (float)i / (float)365 * w;
			float ypos = y;
			float cwidth = (float)(w / ((float) 365 / (float) range));

			column.setX(xpos);
			column.setY(ypos);
			column.setHeight(h);
			column.setWidth(cwidth);
			column.setup();

			daily.add(column);
		}

		calcAverages();
	}

	void calcAverages()
	{	
		if(daily.size() <= 0) return;

		int cellCount = daily.get(0).count();

		for(int y = 0; y < cellCount; y++)
		{
			// total
			float totalHue = 0, totalSaturation = 0, totalBrightness = 0;

			for(int x = 0; x < daily.size(); x++)
			{
				ColorColumn column = daily.get(x);
				ColorCell cell = column.get(y);
				color col = cell.getAverage();
				totalHue += hue(col);
				totalSaturation += saturation(col);
				totalBrightness += brightness(col);
			}

			// averages
			float avgHue = totalHue	/ daily.size();
			float avgSat = totalSaturation / daily.size();
			float avgBright = totalBrightness / daily.size();

			for(int j = 0; j < daily.size(); j++)
			{
				ColorColumn column = daily.get(j);
				ColorCell cell = column.get(y);
				cell.avgHue = avgHue;
				cell.avgSat = avgSat;
				cell.avgBright = avgBright;
			}
		}
	}

	void draw()
	{
		noFill();
		strokeWeight(1);
		stroke(0, 100);
		rect(x, y, w, h);

		for(ColorColumn col : daily)
			col.draw();
	}
}