public class Strophe
{
	protected Verse[] verses;

	public Strophe(Verse[] verses)
	{
		this.verses = verses;
	}

	public Verse[] getVerses()
	{
		return verses;
	}
}

public class Verse
{
	Sentence origin;
	String text;

	public Verse(String text, Sentence origin)
	{
		this.text = text; //quoteLastWord(text);
		this.origin = origin;
	}

	String[] getWords()
	{
		return getWords(text);
	}

	String[] getWords(String txt)
	{
		String t = txt.replace('\n', ' ');
		return splitTokens(t, " ");
	}

	String getText()
	{
		return text;
	}

	String toString()
	{
		return text;
	}

	Author[] getAuthors()
	{
		if(origin == null)
			return null;
		else
			return origin.getAuthors();
	}
}