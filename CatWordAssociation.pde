public class Association extends GenHisto<ArticleStringCollection>
{
	ArrayList<IHisto> histos;
	Alignment align;

	PVector zero, dir;

	public Association(ArticleStringCollection col, Alignment align)
	{
		super(col);
		this.align = align;
	}

	void setup()
	{
		histos = new ArrayList<IHisto>();

		switch(align)
		{
			case RIGHT:
			zero = new PVector(x + w, y + h/2);
			dir = new PVector(-1,0);
			break;

			case LEFT:
			zero = new PVector(x, y+h/2);
			dir = new PVector(1,0);
			break;
		}

		String str = data.getString();
		Textbox box = new Textbox(str);
		box.horizontal = align;
		box.vertical = Alignment.CENTER;
		box.col = color(0);
		box.size = 14;
		if(str.length() > 10)
			box.size = 13;
		box.setWidth(w);
		box.setHeight(18);
		box.setX(x + dir.x * 17);
		box.setY(y + h/3 - box.getHeight()/2);
		box.setup();
		histos.add(box);

		DotCircleHisto palette = new DotCircleHisto(data.get());
		palette.circleSizeMultiplier = .6;
		palette.setWidth(box.getHeight() * .9);
		palette.setHeight(box.getHeight() * .9);
		palette.setY(box.getY() + box.getHeight()/2 + 2.5);

		if(align == Alignment.LEFT)
			palette.setX(zero.x);
		else 			
			palette.setX(zero.x);

		palette.setup();
		histos.add(palette);

		/*EmotionBars emotions = new EmotionBars(articles.get());
		emotions.minified = true;
		emotions.setWidth(30);
		emotions.setHeight(20);
		if(align == Alignment.LEFT)
			emotions.setX(x + w - emotions.getWidth() - 5);
		else 			
			emotions.setX(x + 5);
		emotions.setY(box.getY() - emotions.getHeight()/4 + 5);
		emotions.setup();
		histos.add(emotions);*/
	}

	void draw()
	{
		noFill();
		strokeWeight(1);
		stroke(0, 10);
		//rect(x,y,w,h);

		for(IHisto histo : histos)
			histo.draw();

		switch(align)
		{
			case RIGHT:
			//drawAlignedRight();
			break;

			case LEFT:
			//drawAlignedLeft();
			break;
		}
	}

	void drawAlignedLeft()
	{
		String str = data.getString();
		int count = data.get().size();

		Article a = data.get(0);
		fill(0,150);
		textSize(10);
		text(a.title, x, y + 18, w, h);
	}

	void drawAlignedRight()
	{
		String str = data.getString();
		int count = data.get().size();

		Article a = data.get(0);
		fill(0,150);
		textSize(10);
		text(a.title, x, y + 18, w, h);
	}
}