// graphs per category

public class CatsGraphs extends Histo
{
	public CatsGraphs(ArrayList<Article> articles)
	{
		super(articles);
	}

	ArrayList<IHisto> histos;

	void setup()
	{
		histos = new ArrayList<IHisto>();

		// {"world", "americas", "europe", "middle east", "asia", "arts", "sports"};
		String[] cats = new String[]{"world", "americas", "europe", "middle east", "asia", "sports", "arts"};

		float spacing = 150;
		float totalSpacing = spacing * (cats.length-1);
		float catSize = (w-totalSpacing) / cats.length;

		for(int i = 0; i < cats.length; i++)
		{
			String str = cats[i];

			float catx = x + i * catSize;

			IHisto histo = new CatGraph(articles, str);
			histo.setX(catx + i*spacing);
			histo.setY(y);
			histo.setWidth(catSize);
			histo.setHeight(h);
			histo.setup();
			histos.add(histo);
			
			/*
			IFilter filter = new CategoryFilter(str);
			ArrayList<Article> filtered = (ArrayList<Article>)filter.filter(articles);

			IHisto faces = new FaceGraph(filtered, catx+i*spacing, y, catSize, h/3);
			histos.add(faces);
			*/
			
		}
	}

	void draw()
	{
		noFill();
		strokeWeight(1);
		stroke(0,50);
		//rect(x,y,w,h);
		//line(x, y + h, x + w, y + h);

		for(IHisto histo : histos)
			histo.draw();
	}
}