public class EmotionHisto extends Histo
{
	public Date earliest, oldest;

	ArrayList<Article> dataSet;
	int year;
	String emotion;

	public EmotionHisto(ArrayList<Article> articles, String emotion) 
	{ 
		super(articles); 
		this.emotion = emotion;
		setup();
	}

	HashMap<Integer, ArrayList<Article>> emotionDates;

	void setup()
	{
		year = 2013;
		dataSet = new ArrayList<Article>();
		emotionDates = new HashMap<Integer, ArrayList<Article>>();

		for(Article a : articles)
		{
			Date d = a.pubDate;
			int y = d.getYear();
			if(y == year)
			{
				addArticle(a);
			}
		}
	}

	void addArticle(Article a)
	{
		dataSet.add(a);

		Date d = a.pubDate;
		int days = d.totalDays();
		ArrayList<Article> articles = null;

		if(emotionDates.get(days) != null)
			articles = emotionDates.get(days);
		else  
			articles = new ArrayList<Article>();

		articles.add(a);
		emotionDates.put(days, articles);
	}

	void draw()
	{
		noFill();
		stroke(0, 100);
		rect(x,y,w,h);

		float biggest = 0;

		for(Map.Entry me : emotionDates.entrySet())
		{
			float totalAnger = 0, totalSadness = 0, totalFear = 0, totalDisgust = 0, totalJoy = 0;

			ArrayList<Article> articles = (ArrayList<Article>) me.getValue();

			for(Article a : articles)
			{
				totalAnger += a.getAnger();
				totalSadness += a.getSadness();
				totalFear += a.getFear();
				totalDisgust += a.getDisgust();
				totalJoy += a.getJoy();
			}

			totalAnger /= articles.size();
			totalSadness /= articles.size();
			totalFear /= articles.size();
			totalDisgust /= articles.size();
			totalJoy /= articles.size();

			if(totalAnger > biggest) biggest = totalAnger;
			if(totalSadness > biggest) biggest = totalSadness;
			if(totalFear > biggest) biggest = totalFear;
			if(totalDisgust > biggest) biggest = totalDisgust;
			if(totalJoy > biggest) biggest = totalJoy;
		}



		Article mostIntense = null;

		int d = 0;

		for(Map.Entry me : emotionDates.entrySet())
		{
			float total = 0;
			ArrayList<Article> articles = (ArrayList<Article>) me.getValue();

			float cy = y+h;

			float p = d % 7;
			p *= 4;
			colorMode(HSB, 255);
			color c = color(p, 150, 210);
			colorMode(RGB, 255);

			for(Article a : articles)
			{
				float em = a.getEmotion(emotion);
				total += em;

				int day = (int) me.getKey();
				float px = x + (float)w * (float)day / 365;

				float yHeight = em;// / biggest * h;
				float py = y + h - yHeight;


				fill(c);
				noStroke();
				rect(px, cy, (float)w/365 * .9, -yHeight);
				cy -= (yHeight + 2);
			}

			d += 1;

			total /= articles.size();

		}


		String xDesc = "x = time in " + year;
		String yDesc = "y = \"" + emotion + "\" normalized by article count/day and highest emotion in total";
		String additional = "days covered: " + emotionDates.size() + ", articles: " + articles.size();

		fill(0);
		textAlign(LEFT, TOP);
		textSize(14);
		text(xDesc + ", " + yDesc + ", " + additional, x + 5, y+h+12);
	}
}