public class ImageBox extends GenHisto<PImage>
{
	public ImageBox(PImage img)
	{
		super(img);
	}

	public ImageBox(PImage img, float x, float y, float w, float h)
	{
		super(img, x, y, w, h);
	}

	void setup()
	{
		float ratio = (float)data.height / (float)data.width;
		setHeight(w*ratio);
	}

	void draw()
	{
		image(data, x, y, w, h);
	}
}