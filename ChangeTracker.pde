public interface IChangeTracker<T>
{
	void add(T t);
	T calcResult();
	void flush();
}

// gets emotion values and saves them
// stores most highest
// checks if threshold is reached on calc and returns most intense article
public class EmotionTracker implements IChangeTracker<Article>
{
	float threshold, currentTotal = 0, lastAvg = 0, lastEmotion = 0;

	String emotion;

	ArrayList<Article> articles;
	Article result;

	EmotionTracker(String emotion, float threshold)
	{
		this.emotion = emotion;
		this.threshold = threshold;
		articles = new ArrayList<Article>();
	}

	void add(Article a)
	{
		float value = a.getEmotion(emotion);
		currentTotal += value;
		articles.add(a);
		if(value > lastEmotion)
		{
			result = a;
			lastEmotion = value;
		}
	}

	Article calcResult()
	{
		float avg = currentTotal / articles.size();

		boolean doReturnResult = false;

		if(lastAvg > 0 && avg - lastAvg > threshold && result != null)
		{
			doReturnResult = true;
		}
		else  
		{
			doReturnResult = false;
		}

		lastAvg = avg;

		if(doReturnResult)
			return result;
		else 
			return null;
	}

	void flush()
	{
		currentTotal = 0;
		result = null;
		lastEmotion = 0;
		articles.clear();
	}
}