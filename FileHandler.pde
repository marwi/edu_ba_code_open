import org.apache.commons.io.FilenameUtils;

public String RemoveExtension(String name)
{
	return FilenameUtils.removeExtension(name);
}

public File[] GetAll(String dir)
{
	java.io.File folder = new java.io.File(dataPath(dir));
	File[] files = folder.listFiles();
	return files;
}

public boolean Exists(String path)
{
	File f = new File(path);
	return f.exists() && !f.isDirectory();
}

public String GetName(String pathToFile)
{
	return RemoveExtension(new File(pathToFile).getName());
}

public String GetName(File file)
{
	return RemoveExtension(file.getName());
}

public String GetAbsolutePath(File file)
{
	return file.getAbsolutePath();
}

public String[] getDirectories(String path)
{
	return new File(path).list();
}

public String[] getDirectoriesFull(String path)
{
	String[] dirs = getDirectories(path);
	for(int i = 0; i < dirs.length; i++)
		dirs[i] = path + "/" + dirs[i];
	return dirs;
}