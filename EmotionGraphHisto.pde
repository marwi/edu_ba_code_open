public class EmotionGraphHisto extends Graph
{
	ArrayList<EmotionGraphCell> cells;

	int lineThickness = 3;
	int days = 1;

	public EmotionGraphHisto(ArrayList<Article> articles)
	{
		super(articles);
	}

	void setup()
	{
		// sort by appearance
		Collections.sort(articles, new Comparator<Article>()
		{
			public int compare(Article col1, Article col2)
			{
				int s1 = col1.pubDate.totalDays();
				int s2 = col2.pubDate.totalDays();
				if(s1 > s2) return 1;
				else if(s1 < s2) return -1;
				else return 0;
			}
		});
	}

	void drawGraph()
	{
		// colors for emotions
		drawGraph("anger", color(200, 40, 0, 100), 0);
		drawGraph("fear", color(50, 50, 200, 100), 1);
		drawGraph("disgust", color(180, 170, 0, 100), 2);
		drawGraph("sadness", color(60, 180, 220, 150), 3);
		drawGraph("joy", color(110, 160, 50, 150), 4);
		//draw("fear");
	}


	void drawGraph(String type, color col, int id)
	{
		noStroke();
		fill(col);
		textAlign(RIGHT, CENTER);
		textSize(10);
		text(type, x - 50, y + id * h/5 + h/5/2);

		noFill();
		stroke(col);
		strokeWeight(lineThickness);
		strokeJoin(ROUND);

		beginShape();

		int range = 1;

		for(int i = 1; i <= 365; i += range )
		{
			DayFilter f = new DayFilter(i, range-1);
			ArrayList<Article> filtered = (ArrayList<Article>) f.filter(articles);
			float x = this.x + w * (float)i / (float)365;

			if(filtered.size() >= 1)
			{
				float avg = getAverageEmotion(filtered, type);
				drawVertex(x, avg);
			}
			else
			{
				drawVertex(x, 0);
			}
			
		}

		endShape();
		strokeJoin(MITER);
	}

	void drawVertex(float x, float totalEmotion)
	{
		float y = this.y + h - h * totalEmotion;
		vertex(x, y);
	}

	public class EmotionGraphCell
	{
		public float emotion = 0;
		public ArrayList<Article> articles = new ArrayList<Article>();

		public EmotionGraphCell(int from, int to)
		{

		}

		void add(Article a)
		{
			articles.add(a);
		}
	}
}