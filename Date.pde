public class Date
{
	private int year, month, day, hour, minute, second;

	public Date(String str)
	{
		String[] ds = splitTokens(str, "-T:Z");
		for(String s : ds)
		{
			//println(s);
		}

		for(int i = 0; i < ds.length; i++)
		{
			String s = ds[i];
			switch(i)
			{
				case 0:
				year = Integer.parseInt(s);
				break;

				case 1:
				month = Integer.parseInt(s);
				break;

				case 2:
				day = Integer.parseInt(s);
				break;
				
				case 3:
				hour = Integer.parseInt(s);
				break;
				
				case 4:
				minute = Integer.parseInt(s);
				break;
				
				case 5:
				second = Integer.parseInt(s);
				break;
			}
		}
	}

	public int getYear() { return year; }
	public int getMonth() { return month; }
	public int getDay() { return day; }
	public int getHour() { return hour; }
	public int getMinute() { return minute; }
	public int getSecond() { return second; }

	/*
	Januar 31
	Februar 28
	März 31
	April 30
	Mai 31
	Juni 30
	Juli 31
	August 31
	September 30
	Oktober 31
	November 30
	Dezember 31
	*/

	public int totalDays()
	{
		int total = 0;

		for(int i = 1; i < getMonth(); i++)
		{
			switch(i)
			{
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
				total += 31;
				break;

				case 2:
				total += 28;
				break;

				default:
				total += 30;
			}
		}

		total += getDay();

		return total;
	}

	public String toString() { 
		return getDay() + "." + getMonth() + "." + getYear();
	}

	public String prettyPrint()
	{
		return getDay() + "." + getMonth() + "." + getYear();
	} 
}