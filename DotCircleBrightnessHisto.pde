public class DotCircleBrightnessHisto extends Histo
{
	int maxColors;

	public DotCircleBrightnessHisto(ArrayList<Article> articles)
	{
		super(articles);
		setup();
	}

	void setup()
	{
		maxColors = 5;
	}

	void draw()
	{
		float dia = w;

		noFill();
		fill(255, 50);
		strokeWeight(1);
		stroke(0, 50);
		ellipse(x, y, dia, dia);

		for(Article a : articles)
		{
			for(int i = 0; i < a.paletteSize() && i < maxColors; i++)
			{
				color col = a.getColor(i);

				if(saturation(col) > 100) 
				{
					continue;
				}

				PVector vec = new PVector(1,0);

				float hue = hue(col);
				float angle = brightness(col)/(float)255 * 360;
				float saturation = saturation(col);
				float length = saturation/255 * dia/2;

				vec.mult(length);
				vec.rotate(radians(angle));

				float dotRadius = 4;

				fill(col, 200);
				noStroke();
				ellipse(x+vec.x, y+vec.y, dotRadius, dotRadius);
			}	
		}
	}
}
