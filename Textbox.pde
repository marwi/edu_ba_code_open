public class Textbox extends GenHisto<String>
{
	public Alignment 
	horizontal = Alignment.CENTER, 
	vertical = Alignment.CENTER;

	int size = 14;
	boolean wrap = true;
	color col;

	public Textbox(String text, float x, float y, float w, float h)
	{
		super(text, x, y, w, h);
	}

	public Textbox(String text, Alignment horizontal, Alignment vertical)
	{
		this(text, horizontal);
		this.vertical = vertical;
	}

	public Textbox(String text, Alignment horizontal)
	{
		this(text);
		this.horizontal = horizontal;
	}

	public Textbox(String text)
	{
		super(text);
	}

	void setColor(color col)
	{
		this.col = col;
	}

	float getTextWidth()
	{
		textSize(size);
		return textWidth(data);
	}

	void setup()
	{

	}

	void draw()
	{
		noFill();
		strokeWeight(1);
		stroke(0, 50);
		//rect(x,y,w,h);

		noStroke();
		fill(col);

		float y = this.y;

		int alignx = CENTER;
		switch(horizontal)
		{
			case LEFT:
			alignx = LEFT;
			break;
			case RIGHT:
			alignx = RIGHT;
			break;
		}

		int aligny = CENTER;
		switch(vertical)
		{
			case TOP:
			aligny = TOP;
			if(!wrap)
				y = this.y;
			break;
			case BOTTOM:
			aligny = BOTTOM;
			if(!wrap)
				y = this.y + h;
			break;
			case CENTER:
			aligny = CENTER;
			if(!wrap)
				y = this.y + h/2;
			break;
		}

		textSize(size);
		textAlign(alignx, aligny);

		try
		{
			if(wrap)
				text(data, x, y, w, h);
			else 
				text(data, x, y);
		}
		catch(Exception e)
		{
			println(e);
		}
		
	}
}