public interface ILayout<T, U>
{
	// layouts elements, returns unlayouted elements
	T layout(U elements, float x, float y, float w, float h);
}

public class LeftAligned implements ILayout<ArrayList<IHisto>, ArrayList<IHisto>>
{
	float lineSpacing = 2;
	float histoSpacing = 0;

	public LeftAligned(){}

	public LeftAligned(float lineSpacing)
	{
		this.lineSpacing = lineSpacing;
	}

	public LeftAligned(float lineSpacing, float histoSpacing)
	{
		this.lineSpacing = lineSpacing;
		this.histoSpacing = histoSpacing;
	}

	ArrayList<IHisto> layout(ArrayList<IHisto> data, float x, float y, float w, float h)
	{
		float hx = x;
		float hy = y;

		boolean reachedEnd = false;
		ArrayList<IHisto> leftoverHistos = new ArrayList<IHisto>();

		for(IHisto histo : data)
		{
			// check for linebreak
			if(hx + histo.getWidth()+histoSpacing >= x + w)
			{
				hx = x;
				hy += histo.getHeight() + lineSpacing;
			}

			// check for reached end
			if(hy + histo.getHeight()+lineSpacing >= y + h)
			{
				reachedEnd = true;
			}


			if(!reachedEnd)
			{
				histo.setX(hx);
				histo.setY(hy);
				histo.setup();
				hx += histo.getWidth() + histoSpacing;
			}
			else
			{
				leftoverHistos.add(histo);
			}
		}

		return leftoverHistos;
	}
}