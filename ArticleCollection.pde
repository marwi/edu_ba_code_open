public class ArticleCollection
{
	ArrayList<Article> articles;

	public ArticleCollection(Article a)
	{
		this();
		add(a);
	}

	public ArticleCollection()
	{
		articles = new ArrayList<Article>();
	}

	int size()
	{
		return articles.size();
	}

	void add(Article a)
	{
		articles.add(a);
	}

	void clear()
	{
		articles.clear();
	}

	void remove(Article a)
	{
		articles.remove(a);
	}

	ArrayList<Article> get()
	{
		return articles;
	}

	Article get(int i)
	{
		if(i >= articles.size()) return null;
		else return articles.get(i);
	}
}

public class ArticleStringCollection extends ArticleCollection
{
	String string;

	public ArticleStringCollection(Article a, String str)
	{
		super(a);
		string = str;
	}

	String getString()
	{
		return string;
	}
}