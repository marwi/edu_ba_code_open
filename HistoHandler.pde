public class HistoHandler extends Histo
{
	ArrayList<Article> dataSet;
	ArrayList<IHisto> histos;
	int year = 2015;

	public HistoHandler(ArrayList<Article> articles) 
	{ 
		super(articles);
	}

	void setup()
	{
		Collections.sort(this.articles, new Comparator<Article>(){
			public int compare(Article a1, Article a2) 
			{
				float v1 = a1.pubDate.totalDays();//a1.getEmotion(emotion);
				float v2 = a2.pubDate.totalDays(); //a2.getEmotion(emotion); 	
				if(v1 > v2) return 1;
				else if(v1 < v2) return -1;
				else return 0;			
			}
		});


		IFilter filter = new YearFilter(year);
		dataSet = (ArrayList<Article>) filter.filter(articles);

		average = new ToneAvg();
		average.get(dataSet);

		histos = new ArrayList<IHisto>();

		
		
		if(output == Output.PLAKAT1)
			plakat1();
		else if(output == Output.PLAKAT2)
			plakat2();
		else if(output == Output.ARTICLES)
			articles();
		else if(output == Output.FACES)
			faces();
		else if(output == Output.COVER)
			cover();
		else if(output == Output.RHYMES)
			rhymes();
		
	}

	void articles()
	{
		IHisto detail = new DetailHisto(dataSet);		
		detail.setX(x);
		detail.setY(y);
		detail.setWidth(w);
		detail.setHeight(h);
		detail.setup();
		histos.add(detail);
	}

	void plakat1()
	{
		IHisto images = new ImageSliceHisto(dataSet);
		images.setX(x);
		images.setY(y);
		images.setWidth(w);
		images.setHeight(h*.02);
		images.setup();
		histos.add(images);

		IHisto colors = new ColorHisto(dataSet);
		colors.setX(x);
		colors.setY(images.getY() + images.getHeight() + 60);
		colors.setWidth(w);
		colors.setHeight(h*.3);
		histos.add(colors);

		BrightnessGraph graph = new BrightnessGraph(dataSet);		
		graph.setX(x);
		graph.setY(colors.getY() + colors.getHeight() + 30);
		graph.setWidth(w);
		graph.setHeight(h*.04);
		graph.setup();
		histos.add(graph);

		IHisto emotions = new EmotionGraphHisto(dataSet);
		emotions.setX(x);
		emotions.setY(graph.getY() + graph.getHeight() + 30);
		emotions.setWidth(w);
		emotions.setHeight(graph.getHeight());
		emotions.setup();
		histos.add(emotions);

		ArticleHighlightGraph articleHighlight = new ArticleHighlightGraph(dataSet);		
		articleHighlight.setX(x);
		articleHighlight.setY(emotions.getY() + emotions.getHeight() + 30);
		articleHighlight.setWidth(w);
		articleHighlight.setHeight(emotions.getHeight()*1.5);
		articleHighlight.setup();
		histos.add(articleHighlight);

		IHisto words = new MonthlyWords(dataSet);
		words.setX(x);
		words.setY(articleHighlight.getY() + articleHighlight.getHeight() + 200);
		words.setWidth(w);
		words.setHeight(h*.4);
		words.setup();
		histos.add(words);
	}

	void plakat2()
	{
		IHisto catsGraphs = new CatsGraphs(dataSet);
		catsGraphs.setX(x);
		catsGraphs.setY(y);
		catsGraphs.setWidth(w);
		catsGraphs.setHeight(h);
		catsGraphs.setup();
		histos.add(catsGraphs);
	}

	void faces()
	{
		IHisto faces = new FaceGraph(data, x, y, w, h);
		histos.add(faces);
	}

	void cover()
	{
		DotCircleHisto cover = new DotCircleHisto(dataSet);//Madness(data, x, y, w, h)
		cover.setX(x+w/2);
		cover.setY(y+h/2);
		cover.setWidth(w*.75);
		cover.setHeight(w*.75);
		cover.maxColors = 10;
		cover.circleSizeMultiplier = 1;
		cover.percentageMultiplier = 1;
		cover.alphaMultiplier = .9;
		cover.considerSaturation = true;
		histos.add(cover);
	}

	void rhymes()
	{
		IHisto histo = new Rhymomat(articles, x, y, w, h);
		histos.add(histo);
	}

	void draw()
	{
		if(output != Output.RHYMES)
		{
			stroke(0, 20);
			strokeWeight(1);
			noFill();
			line(x, y+h, x+w, y+h);

			String description =  "time: " + year;
			description += ", articles: " + dataSet.size();
			fill(0);
			textAlign(LEFT, TOP);
			textSize(20);
			text(description, x + 5, y + h + 3);
		}

		for(IHisto histo : histos)
			histo.draw();
	}

	void keyPressed(char key)
	{
		for(IHisto histo : histos)
		{
			histo.keyPressed(key);
		}
	}
}
