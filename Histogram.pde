
public class Histogram
{
	float
	w = width - 100,
	h = height - 200,
	x = 50,
	y = 100;

	String emotion = "anger";

	float xResolution = 1;
	float barWidth = 2; //(int)w/3/(xResolution+1);

	ArrayList<Article> articles = new ArrayList<Article>();

	HashMap<Float,Col> col = new HashMap<Float,Col>();

	public Histogram(String emotion)
	{
		this.emotion = emotion;
	}

	void setSize(float x, float y, float w, float h)
	{
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		xResolution = 16;
		barWidth = w/(xResolution+1); 
	}

	void add(Article article)
	{
		if(article.isValid())
		{
			articles.add(article);

			float emotion = article.getEmotion(this.emotion);
			emotion = (int)(emotion * xResolution);
			emotion = (float) emotion / xResolution;

			addColor(article, emotion);
		}
	}

	void addColor(Article article, float emotion)
	{
		Col c;

		if(this.col.get(emotion) != null)
		{
			c = this.col.get(emotion);
			c.add(article);
		}
		else
		{
			c = new Col(article);
		}
		this.col.put(emotion, c);
	}

	color average(color c1, color c2)
	{
		return lerpColor(c1, c2, 0.5);
	}

	float biggestWordCount;
	void normalize()
	{
		for(Map.Entry me : col.entrySet())
		{
			Col v = (Col)me.getValue();

			for(int i = 0; i < v.count(); i++)
			{
				Article a = v.get(i);
				if(a.getWordCount() > biggestWordCount)
					biggestWordCount = a.getWordCount();
			}
		}
		println(biggestWordCount);
	}

	void sort()
	{
		for(Map.Entry me : col.entrySet())
		{
			Col v = (Col)me.getValue();
			v.sort();
		}
	}

	void draw()
	{
		fill(0,0);
		stroke(0, 50);
		strokeWeight(1);
		rect(x,y,w,h);

		noStroke();

		//drawRGB();
		drawCol(col, 0);

		textAlign(RIGHT, BOTTOM);
		textSize(18);
		fill(0, 200);

		float vx = x - 20;
		float vy = y + h - 5;

		text(emotion, vx, vy);

		stroke(0, 50);
		line(0,y+h,x,y+h);
	}



	void drawCol(HashMap<Float, Col> map, float offsetX)
	{
		for(Map.Entry me : map.entrySet())
		{
			float k = (float)me.getKey();
			Col v = (Col)me.getValue();

			float vx = k * (w - barWidth*1);
			float px = vx + x + offsetX;

			stroke(0, 20);
			noFill();
			rect(px, y, barWidth, h);

			float total = 0;
			int count = 0;

			for(int i = 0; i < v.count(); i++)
			{
				Article a = v.get(i);

				color c = a.getColor(0);
				float ch = (float)h / v.count();
				float vy = i * ch;
				float cw = barWidth / biggestWordCount * a.getWordCount();

				String emotionWidth = "anger";

				cw = barWidth * a.getEmotion(emotionWidth);//a.getEmotion(emotion);

				total += a.getEmotion(emotionWidth);
				count += 1;

				noStroke();
				//stroke(0, 20);// 800/(10+a.getWordCount()));

				fill(c);//, 50 + a.getWordCount()/2);

				float py = h - vy + y - ch;

				rect(px, py, ceil(cw), ceil(ch));
				//ellipse(px+barWidth/2, py+ch/2, barWidth/2, ch);
			}

			float average = total/(float)count;
			/*fill(190, 200);
			float lx = px + average*barWidth;
			rect(px, y, average*barWidth, h);*/
		}
	}
}

public class Col
{
	public Col(Article a)
	{
		add(a);
	}

	ArrayList<Article> articles = new ArrayList<Article>();

	void add(Article a)
	{
		articles.add(a);
	}

	int count()
	{
		return articles.size();
	}

	Article get(int index)
	{
		return articles.get(index);
	}

	void sort()
	{
		sortByAnger();
		//sortByHue();
	}

	void sortByAnger()
	{
		Collections.sort(articles, new Comparator<Article>()
		{
			public int compare(Article a1, Article a2) 
			{
				float e1 = a1.getEmotion("anger");
				float e2 = a2.getEmotion("anger");

				if(e1 > e2) return 1;
				else if(e1 < e2) return -1;
				else return 0;

				//return c;
			}
		});
	}

	void sortByHue()
	{
		Collections.sort(articles, new Comparator<Article>()
		{
			public int compare(Article a1, Article a2) 
			{
				color c1 = a1.getColor(0);
				color c2 = a2.getColor(0);
				float val = hue(c1) - hue(c2);
				return (int) val;
			}
		});
	}
}