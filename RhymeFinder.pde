public abstract class RhymeFinder<T, I, R>
{
	protected T data;
	public RhymeFinder(T data) { this.data = data; }
	public abstract R get(I verse);

	boolean validSentence(Sentence sentence)
	{
		return true;
	}

	boolean wordsAreOk(String[] str)
	{
		if(str.length <= 3 || str.length >= 10) 
			return false;
		else 
			return true;
	}

	boolean lengthIsOK(String str)
	{
		if(str.replace(" ", "").length() <= 3) 
			return false;
		else 
			return true;
	}
}

public class RandomRhyme extends RhymeFinder<ArrayList<Sentence>, Verse, Verse>
{
	public RandomRhyme(ArrayList<Sentence> data) { super(data); }

	Verse get(Verse v)
	{
		int rndId = (int)random(0, data.size()-1);
		Sentence sentence = data.get(rndId);

		if(validSentence(sentence))
		{
			String[] subSentences = sentence.getSubsentences();
			if(subSentences.length <= 1) return get(null);

			rndId = (int)random(0, subSentences.length-1);
			String subSentence = subSentences[rndId];

			String[] words = splitTokens(subSentence, "  .,?-;!");
			//String lastWord = words[words.length-1];

			if(lengthIsOK(subSentence) == false || wordsAreOk(words) == false)
				return get(null);
			else
				return new Verse(subSentence, sentence);
		}
		else 
			return get(v);
	}
}

public class Rhyme extends RhymeFinder<ArrayList<Sentence>, Verse, Verse>
{
	RiLexicon lexicon;

	public Rhyme(ArrayList<Sentence> data) 
	{ 
		super(data); 

		lexicon = new RiLexicon();
		RiTa.SILENT = true;
	}

	Verse get(Verse input)
	{
		String[] verseWords = input.getWords();
		String rhymeWord = verseWords[verseWords.length-1];

		int start = (int)random(0, data.size()/2);
		for(int i = start; i < data.size(); i++)
		{
			Sentence sentence = data.get(i);

			if(validSentence(sentence))
			{
				String[] subs = sentence.getSubsentences();

				for(String sub : subs)
				{	
					if(lengthIsOK(sub) == false) continue;				

					String[] words = splitTokens(sub, " ");

					if(wordsAreOk(words) == false) continue;

					// skip too short and too long sentences

					String lastWord = words[words.length-1];

					// skip too short words
					if(lengthIsOK(lastWord) == false 
						|| rhymeWord.toLowerCase().contains(lastWord.toLowerCase())) 
						continue;

					try
					{
						if(lexicon.isRhyme(rhymeWord, lastWord, true) == true)
						{
							//println("found rhyme: " + rhymeWord + " -> " + lastWord);
							return new Verse(sub, sentence);
						}
					}
					catch(Exception e) 
					{ 
						//println(e); 
					}
				}
			}

		}

		// no rhyme found:
		return null;
	}
}

public class EmotionRhyme extends Rhyme
{
	String emotion;
	float threshold;

	public EmotionRhyme(ArrayList<Sentence> data, String emotion, float threshold)
	{
		super(data);
		this.emotion = emotion;
		this.threshold = threshold;
	}

	public boolean validSentence(Sentence sentence)
	{
		float em = sentence.getEmotion(emotion);
		if(em > threshold)
			return true;
		else
			return false;
	}
}