public interface IHisto
{
	void setX(float x);
	void setY(float y);
	void setWidth(float width);
	void setHeight(float height);

	float getX();
	float getY();
	float getWidth();
	float getHeight();

	void setup();
	void draw();
	void keyPressed(char key);
}

public abstract class GenHisto<T> implements IHisto
{
	float x = 0, y = 0, w = width, h = height;

	protected T articles;
	protected T data;
	public GenHisto(T articles)
	{
		this.articles = articles;
		this.data = articles;
	}

	public GenHisto(T data, float x, float y, float w, float h)
	{
		this.data = data;
		this.articles = data;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		setup();
	}

	void setX(float x) { this.x = x; }	
	void setY(float y) { this.y = y; }
	void setWidth(float width) { this.w = width; }
	void setHeight(float height) { this.h = height; }

	float getX() { return x; }
	float getY() { return y; }
	float getWidth() { return w; }
	float getHeight() { return h; }

	Integer[] months = new Integer[] 
	{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };


	abstract void setup();
	abstract void draw();
	void keyPressed(char key) {}
}

public abstract class Histo extends GenHisto<ArrayList<Article>>
{
	public Histo(ArrayList<Article> articles)
	{
		super(articles);
	}

	public Histo(ArrayList<Article> articles, float x, float y, float w, float h)
	{
		super(articles, x, y, w, h);
	}

	float getAverageEmotion(ArrayList<Article> articles, String type)
	{
		float totalEmotion = 0;
		for(Article a : articles)
			totalEmotion += a.getEmotion(type);
		return totalEmotion/articles.size();
	}

	void setup(){}
	void draw(){}
}

