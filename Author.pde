
public class Author
{
	private String firstName = "", lastName = "";

	public Author(String firstName, String lastName)
	{
		if(firstName != null)
			this.firstName = firstName;
		if(lastName != null)
			this.lastName = lastName;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public boolean hasFullName()
	{
		return firstName.length() > 1 && lastName.length() > 1;
	}
}