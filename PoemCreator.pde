public abstract class AbstractPoemCreator <T>
{
	protected T data;
	public AbstractPoemCreator(T data) { this.data = data; }

	public Poem make()
	{
		Strophe[] strophes = getStrophes();
		Author author = createAuthor(strophes, new AuthorMaker());
		String title = createTitle(strophes, new TitleMaker());
		return new Poem(getType(), strophes, author, title);
	}

	protected Author createAuthor(Strophe[] strophes, ICombinator<Author, Author[]> combinator)
	{
		Author[] allAuthors = new Author[0];
		for(Strophe strophe : strophes)
		{
			for(Verse verse : strophe.getVerses())
			{
				Author[] _authors = verse.getAuthors();
				allAuthors = (Author[]) concat(allAuthors, _authors);
			}
		}

		return combinator.combinate(allAuthors);
	}

	protected String createTitle(Strophe[] strophes, ICombinator<String, Strophe[]> combinator)
	{
		return combinator.combinate(strophes);
	}


	protected abstract PoemType getType();
	protected abstract Strophe[] getStrophes();
}

public class Ballade extends AbstractPoemCreator<ArrayList<Sentence>>
{
	/*
	Ist eine Gedichtart, die keine festgelegte metrische Form hat, aber zumeist drei bis sechs Strophen aufweist. Diese Strophen verfolgen dann zumeist das gleiche Metrum und sind häufig gereimt. In der Regel weisen die Strophen höchstens sechs Verse auf und bestehen mindesten aus drei Zeilen.

	Das wesentliche Merkmal der Ballade ist, dass sie eine Mischform der literarischen Gattungen bildet: in ihr kommen sowohl epische (das Erzählen einer Geschichte), lyrische (formal betrachtet ein Gedicht) als auch dramatische (Monologe, Dialoge) Elemente vor.
	*/

	int stropheCount = 2;

	public Ballade(ArrayList<Sentence> data) 
	{ 
		super(data); 
	}

	public Ballade(ArrayList<Sentence> data, int stropheCount)
	{
		super(data);
		this.stropheCount = stropheCount;
	}

	protected PoemType getType()
	{
		return PoemType.Ballade;
	}

	protected Strophe[] getStrophes()
	{
		ArrayList<Strophe> strophes = new ArrayList<Strophe>();
		
		Strophe refrain = simpleRefrain(data);

		for(int i = 0; i < stropheCount; i++)
		{
			strophes.add(simpleStrophe(data));
			if(i + 1 < stropheCount && stropheCount > 2)
				strophes.add(refrain);
		}

		return strophes.toArray(new Strophe[strophes.size()]);
	}

	protected Strophe simpleRefrain(ArrayList<Sentence> data)
	{
		RandomRhyme rnd = new RandomRhyme(data);
		Verse v1 = rnd.get(null);
		Rhyme rhyme = new Rhyme(data);
		Verse v2 = rhyme.get(v1);
		Verse v3 = rhyme.get(v2);
		if(v2 == null)
		{
			return simpleRefrain(data);
		}
		else
		{
			return new Strophe(new Verse[]{v1, v2, v3, v2});
		}
	}

	protected Strophe simpleStrophe(ArrayList<Sentence> data)
	{
		Verse[] verses = new Verse[0];
		for(int i = 0; i < 2; i++)
			verses = (Verse[]) concat(verses, simpleRhyme(data));
		return new Strophe(verses);
	}

	protected Verse[] simpleRhyme(ArrayList<Sentence> data)
	{
		RandomRhyme rnd = new RandomRhyme(data);
		Verse v1 = rnd.get(null);

		Rhyme rhyme = new Rhyme(data);
		Verse v2 = rhyme.get(v1);

		if(v2 == null) 
		{
			return simpleRhyme(data);
		}
		else
		{
			Verse[] verses = new Verse[2];
			verses[0] = v1;
			verses[1] = v2;
			return verses;
		}
	}
}

public class Sonett extends AbstractPoemCreator<ArrayList<Sentence>>
{
	/*
	Ist ein vierzehnzeiliges Gedicht, das aus zwei vierzeiligen und zwei dreizeiligen Strophen besteht. Die Vierzeiler werden Quartette und die Dreilzeiler Terzette genannt. Charakteristisch ist die Verwendung alternierender (abwechselnde Hebung und Senkung) Versmaße, wobei meist der Jambus verwendet wird. Das Reimschema variiert. Typisch ist ein umarmender Reim im Quartett, wobei die Terzette meist dem Muster cdc/dcd, cde/cde und ccd/eed folgen.
	*/

	public Sonett(ArrayList<Sentence> data) 
	{ 
		super(data); 
	}

	protected PoemType getType()
	{
		return PoemType.Sonett;
	}

	protected Strophe[] getStrophes()
	{
		ArrayList<Strophe> strophes = new ArrayList<Strophe>();


		strophes.add(getStrophe());
		strophes.add(getStrophe());

		Strophe[] terzettes = getTerzette();
		for(Strophe str : terzettes)
			strophes.add(str);


		return strophes.toArray(new Strophe[strophes.size()]);
	}

	protected Strophe getStrophe()
	{
		return new Strophe(umarmenderRhyme());
	}

	protected Strophe[] getTerzette()
	{
		// cdc/dcd
		Strophe[] strophes = new Strophe[2];

		// terzett 1:
		Verse v1, v2, v3;
		RandomRhyme rnd = new RandomRhyme(data);
		v1 = rnd.get(null);
		v3 = new Rhyme(data).get(v1);
		v2 = rnd.get(null);
		strophes[0] = new Strophe(new Verse[]{v1, v2, v3});

		// terzett 2:
		Verse v4, v5, v6;
		v4 = new Rhyme(data).get(v2);
		v5 = new Rhyme(data).get(v3);
		v6 = new Rhyme(data).get(v4);
		strophes[1] = new Strophe(new Verse[]{v4, v5, v6});

		return strophes;
	}

	protected Verse[] umarmenderRhyme()
	{
		// abba;

		Verse v1, v2, v3, v4;

		RandomRhyme rnd = new RandomRhyme(data);
		v1 = rnd.get(null);
		Rhyme rhyme = new Rhyme(data);
		v4 = rhyme.get(v1);

		RandomRhyme rnd2 = new RandomRhyme(data);
		v2 = rnd.get(null);
		Rhyme rhyme2 = new Rhyme(data);
		v3 = rhyme.get(v2);

		if(v4 == null || v3 == null) 
		{
			return umarmenderRhyme();
		}
		else
		{
			Verse[] verses = new Verse[4];
			verses[0] = v1;
			verses[1] = v2;
			verses[2] = v3;
			verses[3] = v4;
			return verses;
		}
	}
}

public class Ode extends AbstractPoemCreator<ArrayList<Sentence>>
{
	/*
	Oden sind Gedichte, die etwas lobpreisen. Sie sind in Strophen gegliedert, weisen dabei allerdings kein festes Reimschema auf. In der Antike folgten Oden zumeist bestimmten Odenstrophen, die einen vorgegebenen Aufbau hatten (alkäische, sapphische oder asklepiadeische Odenstrophe).
	*/

	public Ode(ArrayList<Sentence> data)
	{
		super(data);
	}

	protected PoemType getType()
	{
		return PoemType.Ode;
	}

	protected Strophe[] getStrophes()
	{
		ArrayList<Strophe> strophes = new ArrayList<Strophe>();

		strophes.add(getStrophe());
		strophes.add(getStrophe());
		strophes.add(getStrophe());

		return strophes.toArray(new Strophe[strophes.size()]);
	}

	protected Strophe getStrophe()
	{
		Verse[] verses = new Verse[0];
		int verseCount = (int)random(2,4);
		for(int i = 0; i < verseCount; i++)
		{
			verses = (Verse[]) concat(verses, getVerses());
		}
		return new Strophe(verses);
	}

	protected Verse[] getVerses()
	{
		String emotion = "joy";
		float threshold = .7;

		Verse v1 = new RandomRhyme(data).get(null);

		if(v1.origin.getEmotion(emotion) < threshold)
			return getVerses();
		else
		{
			Verse v2 = new EmotionRhyme(data, emotion, threshold).get(v1);
			if(v2 == null)
				return getVerses();
			else
				return new Verse[]{v1,v2};
		}
	}
}