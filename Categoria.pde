public class Categoria
{
	ArrayList<Article> articles = new ArrayList<Article>();
	HashMap<String, ArrayList<Article>> sections = new HashMap<String, ArrayList<Article>>();
	HashMap<String, ArrayList<Article>> subsections = new HashMap<String, ArrayList<Article>>();

	ArrayList<IHisto> histos = new ArrayList<IHisto>();

	public Categoria()
	{

	}

	int size()
	{
		return articles.size();
	}

	void add(Article a)
	{
		if(a.isValid() == false) return;
		
		articles.add(a);

		String sec = a.section;

		if(sec != null)
		{
			if(sections.get(sec) != null)
			{
				ArrayList<Article> articles = sections.get(sec);
				articles.add(a);
			}
			else  
			{
				ArrayList<Article> arr = new ArrayList<Article>();
				arr.add(a);
				sections.put(sec, arr);	
			}
		}

		String subsec = a.subSection;
		if(subsec != null)
		{
			if(subsections.get(subsec) != null)
			{
				ArrayList<Article> articles = subsections.get(subsec);
				articles.add(a);
			}
			else  
			{
				ArrayList<Article> arr = new ArrayList<Article>();
				arr.add(a);
				subsections.put(subsec, arr);	
			}
		}
	}

	void onAllAdded()
	{
		clean();

		float cellHeight = (height - 200);
		float topSpace = 100, bottomSpace = 100;
		float spacing = 400 / (5);
		float cellWidth = width*.95;
		float x = (width - cellWidth)/2;

		IHisto histo = new HistoHandler(articles);
		histo.setX(x);
		histo.setY(topSpace);
		histo.setWidth(cellWidth);
		histo.setHeight(cellHeight);
		histo.setup();
		histos.add(histo);
	}

	void clean()
	{
		sections = cleanMap(sections);
		subsections = cleanMap(subsections);
	}

	HashMap cleanMap(HashMap<String, ArrayList<Article>> map)
	{
		HashMap<String, ArrayList<Article>> newMap = new HashMap<String, ArrayList<Article>>();
		for(Map.Entry se : map.entrySet())
		{
			String k = (String) se.getKey();
			ArrayList<Article> arr = (ArrayList<Article>)se.getValue();
			if(arr.size() > 190)
			{
				newMap.put(k, arr);
			}
		}

		return newMap;
	}

	void draw()
	{
		int y = 100;

		for(IHisto h : histos)
			h.draw();

		//draw(sections, height/3);
		//draw(subsections, height/3*2);
	}

	void keyPressed(char key)
	{
		for(IHisto histo : histos)
		{
			histo.keyPressed(key);
		}
	}
	
	int GetTotalWordCount(ArrayList<Article> articles)
	{
		int total = 0;

		for(Article a : articles)
		{
			int wc = a.getWordCount();
			total += wc;
		}
		return total;
	}
}