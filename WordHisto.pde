// most frequent words
public class WordHisto extends Histo
{
	List<ArticleStringCollection> adverbs, adjectives, nouns, verbs;

	public WordHisto(ArrayList<Article> articles) 
	{ 
		super(articles);
		setup();
	}

	void setup()
	{
		IFilter articleFilter = new ArticleFilter();
		ArrayList<Article> filtered = (ArrayList<Article>) articleFilter.filter(articles);

		WordFilter filter = new WordFilter("adverb");
		adverbs = filter.filter(filtered);

		filter.wordType = "adjective";
		adjectives = filter.filter(filtered);

		filter.wordType = "noun";
		nouns = filter.filter(filtered);

		filter.wordType = "verb";
		verbs = filter.filter(filtered);
	}

	void draw()
	{
		strokeWeight(1);
		noFill();
		stroke(0, 0);
		rect(x, y, w, h);

		drawColumn(adjectives, 0, w/3);
		drawColumn(nouns, 1, w/3);
		drawColumn(verbs, 2, w/3);
	}

	void drawColumn(List<ArticleStringCollection> collection, int id, float columnWidth)
	{
		float offset = 0;

		if(id < 1) offset += 10;
		else if(id > 1) offset -= 10;

		float x = this.x + columnWidth * id + offset;

		if(id == 1)
		{
			float lx1 = x + 6;
			float lx2 = x + columnWidth - 6;


			noStroke();
			fill(0, 10);
			rect(lx1, y, columnWidth - 12, h);

			stroke(0, 20);
			strokeWeight(1);
			line(lx1, y, lx1, y+h);
			line(lx2, y, lx2, y+h);
		}
		

		float count = 5;
		float cellHeight = h/count;

		for(int i = 0; i < collection.size() && i < count; i++)
		{
			ArticleStringCollection asc = (ArticleStringCollection) collection.get(i);
			drawCell(asc, x, y+cellHeight*i, columnWidth, cellHeight);
		}
	}

	void drawCell(ArticleStringCollection asc, float x, float y, float w, float h)
	{
		noFill();
		stroke(0,50);
		strokeWeight(1);
		//rect(x,y,w,h);

		String str = asc.getString();

		// WORD
		textAlign(CENTER, CENTER);
		textSize(14);
		fill(0);
		text(str, x + w/2, y + 14);

		IHisto dots = new DotDiagram(asc.get());
		dots.setX(x);
		dots.setY(y + 28);
		dots.setWidth(w);
		dots.setHeight(10);
		dots.draw();

		DotCircleHisto histo = new DotCircleHisto(asc.get());
		histo.alphaMultiplier = .9;
		histo.maxColors = 12;
		histo.circleSizeMultiplier = .9;
		histo.background = color(255, 50);
		histo.setX(x+w/2);
		histo.setY(y+84);
		histo.setWidth(80);
		histo.setHeight(80);
		histo.draw();

		/*IHisto dotBrightness = new DotCircleBrightnessHisto(asc.get());
		dotBrightness.setX(x+w/3*2+3);
		dotBrightness.setY(y+70);
		dotBrightness.setWidth(40);
		dotBrightness.setHeight(40);
		dotBrightness.draw();*/

		float emWidth = 75;
		IHisto emotions = new EmotionBars(asc.get());
		emotions.setX(histo.getX() - emWidth/2);
		emotions.setY(histo.getY() + histo.getHeight()/2 + 12);
		emotions.setWidth(emWidth);
		emotions.setHeight(50);
		emotions.draw();
	}
}
