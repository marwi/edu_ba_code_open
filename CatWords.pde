public class CatWords extends Histo
{	
	List<ArticleStringCollection> adjectives, nouns, verbs;
	ArrayList<IHisto> cells;

	public CatWords(ArrayList<Article> articles)
	{
		super(articles);
	}

	void setup()
	{
		cells = new ArrayList<IHisto>();

		WordFilter filter = new WordFilter("noun");
		nouns = filter.filter(articles);

		filter = new WordFilter("verb");
		verbs = filter.filter(articles);

		filter = new WordFilter("adjective");
		adjectives = filter.filter(articles);

		setup(adjectives, "adjectives");
		setup(nouns, "nouns");
		setup(verbs, "verbs");
	}

	void setup(List<ArticleStringCollection> collection, String type)
	{
		int id = 0;
		IFilter filterBefore = null, filterAfter = null;

		int count = 7;
		float columnWidth = w / (float) 3;
		float rowHeight = h/ (float) count;

		for(int i = 0; i < collection.size() && i < count; i++)
		{
			ArticleStringCollection asc = (ArticleStringCollection) collection.get(i);

			switch(type)
			{
				case "adjectives": 
				filterBefore = new RelevantWordFilter(asc.getString(), new String[]{"verb", "adjective"}, WordPosition.BEFORE, 3);
				filterAfter = new RelevantWordFilter(asc.getString(), new String[]{"noun"}, WordPosition.AFTER);
				id = 0;
				break;

				case "nouns":
				filterBefore = new RelevantWordFilter(asc.getString(), new String[]{"noun", "verb", "adjective"}, WordPosition.BEFORE, 1);
				filterAfter = new RelevantWordFilter(asc.getString(), new String[]{"verb", "noun"}, WordPosition.AFTER);
				id = 1;
				break;

				case "verbs":
				filterBefore = new RelevantWordFilter(asc.getString(), new String[]{"noun", "adjective"}, WordPosition.BEFORE, 1);
				filterAfter = new RelevantWordFilter(asc.getString(), new String[]{"noun", "verb"}, WordPosition.AFTER, 3);
				id = 2;
				break;
			}

			float xOffset = 0;
			if(id < 1) xOffset = 20;
			else if(id > 1) xOffset = -20;

			CatWordCell cell = new CatWordCell(asc, filterBefore, filterAfter);
			cell.setX(x + columnWidth*id + xOffset);
			cell.setY(y+rowHeight*i);
			cell.setWidth(columnWidth);
			cell.setHeight(rowHeight);
			if(id == 1)
			{
				cell.alpha = 8;
			}

			cell.setup();
			cells.add(cell);
		}
	}

	void draw()
	{
		noFill();
		strokeWeight(1);
		stroke(0, 20);
		//rect(x,y,w,h);

		for(IHisto cell : cells)
		{
			cell.draw();
		}
	}
}