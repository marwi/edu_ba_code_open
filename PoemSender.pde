import http.requests.*;
import java.net.*;

public class PoemSender
{
	protected String url;	

	public PoemSender(String url)
	{
		this.url = url;
	}

	void send(String verse)
	{
		verse = verse.replace("?", "%3F");
		verse = verse.replace(" ", "%20");
		verse = verse.replace("'", "%27");
		verse = verse.replace("\"", "%22");
		verse = verse.replace("|", "%7C");

		String url = this.url + verse;

		println(url);

		try
		{
			GetRequest get = new GetRequest(url);
			get.send();
		}
		catch(Exception e)
		{
			println(url);
			println(e);
		}
	}


}