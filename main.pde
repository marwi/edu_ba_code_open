import java.util.*;
import java.util.Date;
import processing.pdf.*;
import java.util.Map;
import rita.*;

ArrayList<Histogram> histograms = new ArrayList<Histogram>();
Categoria cats;
EmotionIcons icons;
color backgroundColor = color(252,250,245);//color(243, 242, 240);
ToneAvg average;
PFont normal, bold;
PApplet applet;

enum Alignment
{
	LEFT,
	RIGHT,
	CENTER,
	TOP,
	BOTTOM
}

enum Output
{
	PLAKAT1,
	PLAKAT2,
	ARTICLES,
	FACES,
	COVER,
	RHYMES
}

Output output;
boolean saveHighRes, saveScreenshot;
boolean exitAfterDone;

void settings()
{	
	output = Output.RHYMES;
	saveScreenshot = true;
	saveHighRes = true;
	exitAfterDone = true;

	if(output == Output.PLAKAT1)
	{
		size(5500, 3200);
		//size(4252, 2835);
	}
	else if(output == Output.PLAKAT2)
	{
		size(9400, 2400);
	}
	else if(output == Output.ARTICLES)
	{
		size(2500, 4000);
	}
	else if(output == Output.FACES)
	{
		size(2000, 3500);
	}
	else if(output == Output.COVER)
	{
		size(3000, 4500);
		backgroundColor = color(255);
	}
	else if(output == Output.RHYMES)
	{
		size(1920, 1280);
		saveHighRes = false;
		exitAfterDone = false;
		saveScreenshot = false;
		fullScreen();
		//backgroundColor = color(255);
	}
	//size(1700, 2600, PDF, "test.pdf");
}

void setup()
{
	frameRate(10);
	applet = this;

	icons = new EmotionIcons();

	bold = createFont("Palatino Linotype Bold", 60);//("Stempel Garamond LT Pro Bold", 60); //("Palatino Linotype Bold", 60);
	normal = createFont("Palatino Linotype", 60);//("Stempel Garamond LT Pro", 60);
	textFont(normal);

	background(0);

	cats = new Categoria();
	String[] directories = getDirectoriesFull("D:/Personal/Uni/2016/UrbaneEbenen/nyt_downloads");
	int i = 0, j = 0;
	for(String str : directories)
	{
		Article a = new Article(str);
		j ++;
		if(!a.isValid()) continue;
		cats.add(a);
		i ++;
		println(i + "/" + j + " - " + str);

		//if(i >= 3000) break;
	}
	cats.onAllAdded();

	draw();
	
	if(saveHighRes)
		saveHighRes((int)3);
	if(saveScreenshot)
		saveScreenshot();

	if(exitAfterDone)
		exit();
	
}

void draw()
{
	smooth();
	background(backgroundColor);

	if(cats != null)
	{
		if(output != Output.RHYMES)
		{
		}
			fill(0,50);
			textAlign(LEFT, TOP);
			textSize(14);
			text(cats.size(), 5, 5);

		cats.draw();
	}
}

void keyPressed()
{
	cats.keyPressed(key);

	switch(key)
	{
		case 's':
		saveScreenshot();
		break;

		// backspace
		case '\b':
		break;

		case '+':
		break;

		case '-':
		break;

		case ' ':
		break;

		// key
		default:
		break;
	}
}

String timestamp()
{
	return nf(year(),4) + nf(month(),2) + nf(day(), 2) + "-" + nf(hour(),2) + nf(minute(),2) + nf(second(),2);
}

void saveScreenshot()
{
	String name = "frames/output-";
	String time = timestamp();
	saveFrame(name + time + ".png");
}

void saveHighRes(int scaleFactor) 
{
	PGraphics hires = createGraphics(width*scaleFactor, height*scaleFactor, JAVA2D);
	beginRecord(hires);
	hires.smooth();
	hires.textFont(normal, 60);
	hires.scale(scaleFactor);
	draw();
	endRecord();
	String name = "frames/output-";
	String time = timestamp();
	hires.save(name + time + "-hires.png");
}