public class DateHisto extends Histo
{
	public Date earliest, oldest;

	ArrayList<Article> dataSet;
	int year;

	public DateHisto(ArrayList<Article> articles) { super(articles); setup(); }

	void setup()
	{
		year = 2013;
		dataSet = new ArrayList<Article>();

		println("setting up");
		println("articles: " + articles.size());

		for(Article a : articles)
		{
			Date d = a.pubDate;
			int y = d.getYear();
			println(y + " - " + year);
			if(y == year)
			{
				dataSet.add(a);
			}
		}
	}

	void draw()
	{
		noFill();
		stroke(0, 100);
		rect(x,y,w,h);

		int colorCount =  3;

		for(Article a : dataSet)
		{
			for(int i = 0; i < colorCount; i++)
			{
				color c = a.getColor(i);

				if(c == -1)
					continue;

				Date d = a.pubDate;
				int total = d.totalDays();
				float px = x + (float)w * total / 365;

				float val =(float) brightness(c);
				float max = 255;
				float py = y + h - val/max * h;

				fill(c, 255);
				stroke(0, 100);
				strokeWeight(.2);
				ellipse(px, py, 7, 7);
			}
		}

		String xDesc = "x = time in " + year;
		String yDesc = "y = brightness";
		String additional = "total: " + dataSet.size() + "\n" + "verwendet wird nur die "+colorCount+" häufigste Farbe im Bild";

		fill(0);
		textAlign(LEFT, TOP);
		textSize(20);
		text(xDesc + "\n" + yDesc + "\n" + additional, x + 5, y+h+20);
	}
}