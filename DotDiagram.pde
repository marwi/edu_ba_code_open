public class DotDiagram extends Histo
{
	int oneDotRepresents = 5;
	String align = "CENTER";

	public DotDiagram(ArrayList<Article> asc, String align)
	{
		this(asc, 5, align);
	}

	public DotDiagram(ArrayList<Article> asc, int oneDotRepresents, String align)
	{
		this(asc, oneDotRepresents);
		this.align = align;
	}

	public DotDiagram(ArrayList<Article> asc, int oneDotRepresents)
	{
		this(asc);
		this.oneDotRepresents = oneDotRepresents;
	}

	public DotDiagram(ArrayList<Article> asc)
	{
		super(asc);
	}

	float x, y, w, h;
	void setX(float x) { this.x = x; }
	void setY(float y) { this.y = y; }
	void setWidth(float w) { this.w = w; }
	void setHeight(float h) { this.h = h; }

	float getX() { return x; }
	float getY() { return y; }
	float getWidth() { return w; }
	float getHeight() { return h; }

	void setup()
	{

	}

	void draw()
	{
		int count = ceil((float)articles.size() / (float) oneDotRepresents);

		float cellSize = 7;
		float totalWidth = count * cellSize;
		float radius = 4;

		for(int j = 0; j < count; j++)
		{
			float dx = x + w/2 - totalWidth / 2 + cellSize * j;		

			if(align == "LEFT")
			{
				dx = x + cellSize*j;
			}

			float dy = y + h/2;

			fill(0, 100);

			if((j+1)%5 == 0)
				fill(0);

			noStroke();
			ellipse(dx + cellSize/2, dy, radius, radius);
		}

	}
}