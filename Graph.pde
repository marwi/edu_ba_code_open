public abstract class Graph extends Histo
{
	public Graph(ArrayList<Article> articles)
	{
		super(articles);
	}

	void setup() 
	{

	}

	void draw()
	{
		noFill();
		stroke(0,50);
		strokeWeight(1);
		rect(x,y,w,h);

		stroke(200);
		strokeWeight(1);
		//line(x, y+h, x+w, y+h);

		//drawDotTexture();
		drawMonthMarker();
		drawDashedLines();
		drawGraph();
	}

	void drawDotTexture()
	{
		noStroke();
		fill(0, 50);

		for(int x = 0; x < w; x += 3)
		{
			for(int y = 0; y < h; y += 3)
			{
				rect(this.x + x, this.y + y, 1, 1);
			}
		}
	}

	void drawDashedLines()
	{
		stroke(180);
		strokeWeight(1);
		float dashLength = 8;
		float dashSpacing = 6;

		float rootX = x + dashSpacing/2;

		for(int i = 0; i < (w-dashSpacing)/(dashLength+dashSpacing); i++)
		{
			float dx = rootX + i * (dashLength + dashSpacing);

			line(dx, y+h/2, dx+dashLength, y+h/2);
			line(dx, y+h/4, dx+dashLength, y+h/4);
			line(dx, y+h/4*3, dx+dashLength, y+h/4*3);
		}

		textSize(11);
		textAlign(RIGHT, CENTER);
		fill(0, 100);
		String[] marker = new String[]{"0.75", "0.50", "0.25"};
		for(int j = 0; j < marker.length; j++)
		{
			String str = marker[j];
			float x = this.x - 10;
			float y = this.y + j * h/4 + h/4;
			text(str, x, y);
		}
	}

	void drawMonthMarker()
	{
		noFill();
		strokeWeight(4);
		stroke(225);
		//stroke(backgroundColor);
		strokeCap(SQUARE);

		int days = 0;
		for(int i = 0; i < months.length-1; i++)
		{
			int day = months[i];
			days += day;
			float dx = x + w * (float)days/(float)365;
			line(dx, y, dx, y+h);
		}
	}

	abstract void drawGraph();
}