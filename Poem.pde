// www.rhetoriksturm.de/gedichtarten.php
enum PoemType
{
	Ballade,
	Sonett,
	Ode;

	@Override
	public String toString() 
	{
		switch(this) {
			case Ballade: return "ballade";
			case Sonett: return "sonett";
			case Ode: return "ode";
			default: throw new IllegalArgumentException();
		}
	}
}

public class Poem
{
	protected PoemType type;
	//protected Strophe[] strophes;
	protected Strophe[] strophes;
	protected Author author;
	protected String title;

	public Poem(PoemType type, Strophe[] strophes, Author author, String title)
	{
		this.type = type;
		this.strophes = strophes;
		this.author = author;
		this.title = title;
	}

	public PoemType getType()
	{
		return type;
	}

	public Strophe[] getStrophes()
	{
		return strophes;
	}

	public Verse[] getVerses()
	{
		Verse[] verses = new Verse[0];
		for(Strophe strophe : strophes)
		{
			verses = (Verse[])concat(verses, strophe.getVerses());
		}
		return verses;
	}

	public String getText()
	{
		Verse[] verses = getVerses();
		String txt = "";
		for(Verse v : verses)
		{
			txt += v.getText() + " - ";
		}
		return txt;
	}

	public Author getAuthor()
	{
		return author;
	}

	public String getTitle()
	{
		return title;
	}

	public String getCredits()
	{
		String start = "";

		String first = type.toString().substring(0, 1);
		switch(first)
		{
			case "a":
			case "e":
			case "i":
			case "o":
			case "u":
			start = "an";
			break;

			default:
			start = "a";
			break;
		}

		return start + " " + type + " by " + author.firstName + " " + author.lastName;
	}
}

