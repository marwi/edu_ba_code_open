public class Rhymomat extends Histo
{
	RiLexicon lexicon;
	Verse v1, v2;
	PoemSender sender;
	ThemeCollector themes;
	ArrayList<IHisto> texts;
	Poem poem;

	boolean autoRun = true;

	public Rhymomat(ArrayList<Article> articles, float x, float y, float w, float h)
	{
		super(articles, x, y, w, h);
	}

	void setup()
	{
		texts = new ArrayList<IHisto>();

		IFilter f = new ArticleFilter();
		data = articles = (ArrayList<Article>) f.filter(data);

		IFilter f2 =new YearFilter(2015);
		data = articles = (ArrayList<Article>) f2.filter(data);

		themes = new ThemeCollector(data);
		sender = new PoemSender("http://marwi.gienah.uberspace.de:65076/say/");
	}

	int i = 0;
	void draw()
	{
		if(autoRun)
		{
			autoRun();
		}
		else
		{
			for(IHisto text : texts)
				text.draw();
		}
	}



	boolean useThemes = false;
	String theme = "";

	void autoRun()
	{
		boolean doShit = i % 10 == 0;
		if(doShit)
		{
			theme = getRandomTheme();
			println("theme is " + theme);
			try
			{
				ArrayList<Sentence> sentences = themes.getAllSentences();
				//ArrayList<Sentence> sentences = themes.getSentencesAbout(new String[]{theme});
				
				if(sentences.size() > 0)
				{
					AbstractPoemCreator creator = getRandomPoemCreator(sentences);
					makePoem(creator);
				}
				else
				{
					println("UPS! How am I supposed to create a poem from nothing??? sentences: " + sentences.size());
				}
			}
			catch(Exception e)
			{
				println("error " + e);
			}
			
		}

		for(IHisto text : texts)
			text.draw();

		if(doShit)
		{
			//saveScreenshot();
		}

		i++;
	}

	void keyPressed(char key)
	{
		ArrayList<Sentence> sentences;
		AbstractPoemCreator creator;

		switch(key)
		{
			case '1':
			backgroundColor = color(255,255,255);
			sentences = themes.getAllSentences();//themes.getForRandomCategory();
			creator = getRandomPoemCreator(sentences);//new Ballade(sentences, 2);
			makePoem(creator);
			break;


			case '2':
			texts.clear();
			break;

			case '3':
			texts.clear();
			backgroundColor = color(125, 220, 108);
			Textbox thanks = new Textbox("Dankeschön für eure Aufmerksamkeit", x, y, w, h * .8);
			thanks.horizontal = Alignment.CENTER;
			thanks.vertical = Alignment.CENTER;
			thanks.size = 60;
			texts.add(thanks);
			sender.send("Dankeschön für eure Aufmerksamkeit!");
			break;

			case '4':
			backgroundColor = color(255,255,255);
			autoRun = !autoRun;
			break;

			case 't':
			backgroundColor = color(255,255,255);
			sentences = themes.getSentencesAbout(new String[]{" trump ", " clinton ", " obama " ,  " merkel ", " germany ", " america ", " europe "});
			creator = getRandomPoemCreator(sentences);//new Ballade(sentences, 2);
			makePoem(creator);
			break;
		}
	}

	String getRandomTheme()
	{
		String[] themes = new String[]{
			"trump",
			"merkel",
			"obama",
			"crisis",
			"america",
			"germany",
			"europe",
			"clinton",
			"times",
			"berlin",
			"china",
			"nuclear",
			"happy",
			"patriots",
			"leader",
			"city",
			"killed",
			"virtual",
			"new",
			"old",
			"nature",
			"warming",
			"global"
		};

		int rnd = (int)random(0, themes.length-1);
		return themes[rnd];
	}


	void makePoem(AbstractPoemCreator creator)
	{
		try
		{
			// need to fix this method to have a breaking mechanishm to avoid too much recursion!!!!
			poem = creator.make();
			savePoem(poem);
			PoemLayout layout = new PoemLayout();
			texts = (ArrayList<IHisto>) layout.layout(poem, x + 20, y, w - 20, h);

			Verse[] verses = poem.getVerses();
			String all = poem.getTitle() + ". " + poem.getCredits() + ". ";
			for(Verse v : verses) 
				all += v.text + ". ";
			sender.send(all);
		}
		catch(Exception e)
		{
			println("error making a poem: " + e);
		}
	}

	void savePoem(Poem poem)
	{
		JSONObject json = new JSONObject();
		json.setString("title", poem.getTitle());
		json.setString("type", poem.getType().toString());
		Author author = poem.getAuthor();
		json.setString("author", author.getFirstName() + " " + author.getLastName());
		json.setString("credits", poem.getCredits());
		json.setString("plain", poem.getText());

		JSONArray strophen = new JSONArray();
		int i = 0;
		for(Strophe s : poem.getStrophes())
		{
			JSONArray strophe = new JSONArray();
			int j =0 ;
			for(Verse v : s.getVerses())
			{
				JSONObject verse = new JSONObject();
				verse.setString("text", v.getText());
				Sentence origin = v.origin;
				Article article = origin.article;
				verse.setString("origin", article.url);
				verse.setString("originalSentence", origin.text);

				JSONObject emotions = new JSONObject();
				emotions.setFloat("anger", origin.getAnger());
				emotions.setFloat("disgust", origin.getDisgust());
				emotions.setFloat("fear", origin.getFear());
				emotions.setFloat("joy", origin.getJoy());
				emotions.setFloat("sadness", origin.getSadness());
				verse.setJSONObject("emotions", emotions);

				ArrayList<ColorInfo> colors = article.colors;
				if(colors.size() > 0)
				{
					color mainColor = colors.get(0).get();
					JSONObject col = new JSONObject();
					col.setFloat("r", red(mainColor));
					col.setFloat("g", green(mainColor));
					col.setFloat("b", blue(mainColor));
					verse.setJSONObject("mainColor", col);
				}

				strophe.setJSONObject(j, verse);
				j++;
			}
			strophen.setJSONArray(i, strophe);
			i++;
		}
		json.setJSONArray("strophen", strophen);

		if(useThemes)
			saveJSONObject(json, "poems/"+data.size()+"/"+theme+"/"+poem.getType().toString()+"/"+poem.getTitle()+".json");
		else
		{
			saveJSONObject(json, "poems/"+data.size()+"/"+poem.getType().toString()+"/"+poem.getTitle()+".json");
		}
	}

	AbstractPoemCreator getRandomPoemCreator(ArrayList<Sentence> sentences)
	{
		float rnd = random(0,1);

		if(rnd < .3)
		{
			return new Ballade(sentences, (int)random(2,3.4));
		}
		else if(rnd < .6)
		{
			return new Sonett(sentences);	
		}
		else
			return new Ode(sentences);
	}

	String[] getWords(String txt)
	{
		String t = txt.replace('\n', ' ');
		return splitTokens(t, " ");
	}

	String quoteLastWord(String text)
	{
		String[] words = getWords(text);
		if(words.length <= 2) return quote(text);

		String beforeLast = words[words.length-2];
		beforeLast = "\"" + beforeLast;
		words[words.length-2] = beforeLast;

		String last = words[words.length-1];
		last = last + "\"";
		words[words.length-1] = last;

		String full = "";
		for(String w : words) 
			full += w + " ";

		return full;
	}

	String quote(String str)
	{
		return "\"" + str + "\"";
	}
}

