// D:\Personal\Uni\2016\UrbaneEbenen\nyt_downloads\4fd2a5688eb7c8105d88cc28

public class ToneData
{
	float anger, disgust, fear, joy, sadness;
	float analytical, confident, tentative;
	float openness, conscientiousness, extraversion, agreeableness, emotionalRange;

	void readTone(JSONArray toneCategories)
	{
		JSONArray tonesEmotions = toneCategories.getJSONObject(0).getJSONArray("tones");
		JSONArray tonesWriting = toneCategories.getJSONObject(1).getJSONArray("tones"); 
		JSONArray tonesSocial = toneCategories.getJSONObject(2).getJSONArray("tones"); 

		anger = tonesEmotions.getJSONObject(0).getFloat("score");
		disgust = tonesEmotions.getJSONObject(1).getFloat("score");
		fear = tonesEmotions.getJSONObject(2).getFloat("score");
		joy = tonesEmotions.getJSONObject(3).getFloat("score");
		sadness = tonesEmotions.getJSONObject(4).getFloat("score");

		analytical = tonesWriting.getJSONObject(0).getFloat("score");
		confident = tonesWriting.getJSONObject(1).getFloat("score");
		tentative = tonesWriting.getJSONObject(2).getFloat("score");

		openness = tonesSocial.getJSONObject(0).getFloat("score");
		conscientiousness = tonesSocial.getJSONObject(1).getFloat("score");
		extraversion = tonesSocial.getJSONObject(2).getFloat("score");
		agreeableness = tonesSocial.getJSONObject(3).getFloat("score");
		emotionalRange = tonesSocial.getJSONObject(4).getFloat("score");
	}

	float getEmotion(String emotion)
	{
		if(emotion == null)
			return 0;
		else 
		{
			try 
			{
				switch(emotion)
				{
					case "anger": return anger;
					case "disgust" : return disgust;
					case "fear" : return fear;
					case "joy" : return joy;
					case "sadness" : return sadness;

					case "analytical": return analytical;
					case "confident" : return confident;
					case "tentative" : return tentative;

					case "openness" : return openness;
					case "conscientiousness" : return conscientiousness;
					case "extraversion" : return extraversion;
					case "agreeableness" : return agreeableness;
					case "emotionalRange" : return emotionalRange;

					default: return 0;
				}
			}
			catch(Exception e)
			{
				println("ToneData: " + e);
				return 0;
			}
		}
	}

	float getAnger() { return anger; }
	float getDisgust() { return disgust; }
	float getFear() { return fear; }
	float getJoy() { return joy; }
	float getSadness() { return sadness; }

	float getEmotionMultiplied(String emotion, float multiplier)
	{
		return getEmotion(emotion) * multiplier;
	}
}

