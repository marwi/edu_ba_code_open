public class ColorPaletteHisto extends GenHisto<Article>
{
	public ColorPaletteHisto(Article article)
	{
		super(article);
	}

	void setup()
	{

	}

	void draw()
	{
		float cx = x;
		float cy = y;

		float spacing = 2;
		float colorWidth = w / 10 - spacing;

		for(int i = 0; i < data.paletteSize() && i < 100; i++)
		{
			if(cx + colorWidth > x + w)
			{
				cx = x;
				cy += colorWidth + spacing*2;
			}

			if(cy + colorWidth > y + h)
			{
				break;
			}

			color col = data.getColor(i);

			fill(col);
			strokeWeight(.2);
			stroke(0, 50);
			ellipse(cx + colorWidth/2, cy+colorWidth/2, colorWidth, colorWidth);

			cx += colorWidth + spacing;
		}	
	}
}