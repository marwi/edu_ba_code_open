
public interface JsonWriter<T>
{
	void Save(T object, String path);
}

public interface JsonReader<T>
{
	T Read(String path);
}

public interface JsonHandler<T>
{
	T Read(String path);
	void Save(T object, String path);
}