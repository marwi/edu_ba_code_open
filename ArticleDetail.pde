// select interesting articles to display
// and arrange them
public class DetailHisto extends Histo
{
	ArrayList<IHisto> histos;

	public DetailHisto(ArrayList<Article> articles)
	{
		super(articles);
	}

	void setup()
	{
		histos = new ArrayList<IHisto>();

		IFilter filter = new ArticleFilter();
		ArrayList<Article> filtered = (ArrayList<Article>) filter.filter(data);

		filter = new MonthFilter(12);
		//filtered = (ArrayList<Article>) filter.filter(filtered);

		filter = new CategoryFilter("asia");
		//filtered = (ArrayList<Article>) filter.filter(filtered);

		filter = new WordCountFilter(120, 1000);
		filtered = (ArrayList<Article>) filter.filter(filtered);

		filter = new StringFilter("berlin");//("asia");
		filtered = (ArrayList<Article>) filter.filter(filtered);

		int i = 0;
		float lastHistoHeight = 0;
		for(Article a : filtered)
		{
			IHisto histo = new ArticleDetail(a);
			histo.setX(x + w*.1);
			histo.setHeight(500);
			histo.setY(y + i * histo.getHeight() + 120);
			histo.setWidth(w*.8);
			histo.setup();
			histos.add(histo);

			lastHistoHeight = histo.getHeight();
			i+=1;

			if(i > 20) break;
		}
	}

	void draw()
	{
		for(IHisto histo : histos)
			histo.draw();
	}
}

public class ArticleDetail extends GenHisto<Article>
{
	ArrayList<IHisto> histos;

	public ArticleDetail(Article article)
	{
		super(article);
	}

	void setup()
	{
		histos = new ArrayList<IHisto>();

		String title = data.title;
		String paragraph = data.leadParagraph;
		String text = data.text;
		String date = data.pubDate.prettyPrint();
		PImage image = data.getImage();

		float anger = data.getAnger();
		float disgust = data.getDisgust();
		float fear = data.getFear();
		float sadness = data.getSadness();
		float joy = data.getJoy();
		String emotions = data.documentType + " - anger: " + anger + ", disgust: " + disgust + ", fear: " + fear + ", sadness: " + sadness + ", joy: " + joy;

		ImageBox img = new ImageBox(image);
		img.setX(x);
		img.setY(y);
		img.setHeight(100);
		img.setWidth(120);
		img.setup();
		histos.add(img);

		ColorPaletteHisto palette = new ColorPaletteHisto(data);
		palette.setWidth(img.getWidth());
		palette.setHeight(80);
		palette.setX(img.getX());
		palette.setY(img.getY() + img.getHeight() + 30);
		palette.setup();
		histos.add(palette);

		DotCircleHisto circleHisto = new DotCircleHisto(data);
		circleHisto.setWidth(palette.getWidth() * .75);
		circleHisto.setHeight(circleHisto.getWidth());
		circleHisto.setX(palette.getX() + palette.getWidth()/2);
		circleHisto.setY(palette.getY() + palette.getHeight() + circleHisto.getHeight()/2 + 10);
		circleHisto.maxColors = 40;
		circleHisto.circleSizeMultiplier = 1.3;
		circleHisto.alphaMultiplier = 1.25;
		circleHisto.setup();
		histos.add(circleHisto);

		Textbox titleBox = new Textbox(title, Alignment.LEFT, Alignment.TOP);
		titleBox.size = 16;
		titleBox.setX(img.getX() + img.getWidth() + 80);
		titleBox.setY(y);
		titleBox.setWidth(w);
		titleBox.setHeight(20);
		titleBox.setup();
		histos.add(titleBox);

		Textbox dateBox = new Textbox(date, 
			titleBox.getX() + titleBox.getTextWidth() + 8, 
			titleBox.getY()-2, 
			55, 
			titleBox.getHeight());
		dateBox.vertical = Alignment.BOTTOM;
		dateBox.size = 10;
		histos.add(dateBox);

		EmotionBarChart chart = new EmotionBarChart(data, 
			dateBox.getX() + dateBox.getTextWidth() + 30, 
			titleBox.getY() - 12, 
			55, 
			titleBox.getHeight() + 8);
		histos.add(chart);

		/*Textbox emotionBox = new Textbox(emotions, Alignment.LEFT, Alignment.TOP);
		emotionBox.size = 12;
		emotionBox.setX(titleBox.getX());
		emotionBox.setY(titleBox.getY() - 40);
		emotionBox.setHeight(16);
		emotionBox.setWidth(w);
		emotionBox.setup();
		histos.add(emotionBox);*/

		ArticleTextEmotion textEmotion = new ArticleTextEmotion(data.sentences);
		textEmotion.setX(titleBox.getX());
		textEmotion.setY(titleBox.getY() + titleBox.getHeight() + 20);
		textEmotion.setHeight(260);
		textEmotion.setWidth(w - textEmotion.getX() - 100);
		textEmotion.setup();
		histos.add(textEmotion);

		//setHeight(textEmotion.getY() + textEmotion.getHeight());
	}

	void draw()
	{
		for(IHisto histo : histos)
		{
			histo.draw();
		}
	}
}

public class ArticleTextEmotion extends GenHisto<ArrayList<Sentence>>
{
	// handles article text emotion coloring and layouting

	Layouter layout;

	public ArticleTextEmotion(ArrayList<Sentence> sentences)
	{
		super(sentences);
	}

	void setup()
	{
		ArrayList<IHisto> histos = new ArrayList<IHisto>();

		float textHeight = 12;
		float textBoxHeight = 14;
		float lineHeight = textBoxHeight;

		float textX = x;
		float textY = y;

		for(Sentence sentence : data)
		{
			String[] words = sentence.getWords();

			// Getting Highest emotion for sentence
			float anger = sentence.getAnger();
			float fear = sentence.getFear();
			float sadness = sentence.getSadness();
			float joy = sentence.getJoy();
			float disgust = sentence.getDisgust();

			String emotionType = "";
			float emotionValue = 0;
			float minEmotionValue = .5;

			if(anger >= minEmotionValue && anger > emotionValue)
			{
				emotionType = "anger";
				emotionValue = anger;
			}
			if(fear >= minEmotionValue && fear > emotionValue)
			{
				emotionType = "fear";
				emotionValue = fear;
			}
			if(disgust >= minEmotionValue && disgust > emotionValue)
			{
				emotionType = "disgust";
				emotionValue = disgust;
			}
			if(sadness >= minEmotionValue && sadness > emotionValue)
			{
				emotionType = "sadness";
				emotionValue = sadness;
			}
			if(joy >= minEmotionValue && joy > emotionValue)
			{
				emotionType = "joy";
				emotionValue = joy;
			}
			// end getting highest emotion

			for(String word : words)
			{
				TextEmotionData data = new TextEmotionData(word, emotionValue, emotionType);

				TextEmotion textEmotion = new TextEmotion(data);
				textEmotion.setX(textX);
				textEmotion.setY(textY);
				textEmotion.setHeight(textBoxHeight);
				textEmotion.setup();
				histos.add(textEmotion);

				textX = textX + textEmotion.getWidth();

				if(textX > x+w-100)
				{
					textX = x;
					textY += lineHeight;
				}
			}
		}

		layout = new ColumnsLayout(histos, x, y, w, h);
		//setHeight(textY - y);
	}

	void draw()
	{
		layout.draw();
	}
}