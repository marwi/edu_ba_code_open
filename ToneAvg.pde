public class ToneAvg
{
	float anger, fear, sadness, disgust, joy;
	float minAnger, minFear, minSadness, minDisgust, minJoy;
	float maxAnger, maxFear, maxSadness, maxDisgust, maxJoy;

	public void get(ArrayList<Article> articles)
	{
		anger = 0;
		fear = 0;
		sadness = 0;
		disgust = 0;
		joy = 0;

		for(Article a : articles)
		{
			float aAnger = a.getAnger(),
				aFear = a.getFear(),
				aSadness = a.getSadness(),
				aDisgust = a.getDisgust(),
				aJoy = a.getJoy();

			if(aAnger < minAnger) minAnger = aAnger;
			if(aFear < minFear) minFear = aFear;
			if(aSadness < minSadness) minSadness = aSadness;
			if(aDisgust < minDisgust) minDisgust = aDisgust;
			if(aJoy < minJoy) minJoy = aJoy;

			if(aAnger > maxAnger) maxAnger = aAnger;
			if(aFear > maxFear) maxFear = aFear;
			if(aSadness > maxSadness) maxSadness = aSadness;
			if(aDisgust > maxDisgust) maxDisgust = aDisgust;
			if(aJoy > maxJoy) maxJoy = aJoy;

			anger += aAnger;
			fear += aFear;
			sadness += aSadness;
			disgust += aDisgust;
			joy += aJoy;
		}

		anger /= articles.size();
		fear /= articles.size();
		sadness /= articles.size();
		disgust /= articles.size();
		joy /= articles.size();
	}
}