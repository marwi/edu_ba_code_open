public class ArticleHighlightGraph extends Graph
{
	public ArticleHighlightGraph(ArrayList<Article> articles)
	{
		super(articles);
	}

	void setup()
	{
		// sort by pub date
		Collections.sort(articles, new Comparator<Article>()
		{
			public int compare(Article col1, Article col2)
			{
				int s1 = col1.pubDate.totalDays();
				int s2 = col2.pubDate.totalDays();
				if(s1 > s2) return 1;
				else if(s1 < s2) return -1;
				else return 0;
			}
		});
	}

	void draw()
	{
		stroke(0, 30);
		noFill();
		strokeWeight(1);
		
		/*rect(x,y,w,h);
		drawMonthMarker();*/

		drawGraph();
	}

	void drawGraph()
	{
		IChangeTracker fearTracker = new EmotionTracker("fear", .19);
		IChangeTracker angerTracker = new EmotionTracker("anger", .19);
		IChangeTracker joyTracker = new EmotionTracker("joy", .1);
		IChangeTracker disgustTracker = new EmotionTracker("disgust", .19);

		for(int i = 1; i <= 365; i++)
		{	
			float x = this.x + w * (float)i / (float)365;
			float y = this.y + 5;

			DayFilter f = new DayFilter(i);
			ArrayList<Article> filtered = (ArrayList<Article>) f.filter(articles);

			for(Article a : filtered)
			{
				fearTracker.add(a);
				angerTracker.add(a);
				joyTracker.add(a);
				disgustTracker.add(a);
			}


			Article anger = (Article) angerTracker.calcResult();
			angerTracker.flush();
			if(anger != null)
				drawArticle(anger, x, y, color(200, 40, 0, 180));

			Article fear = (Article) fearTracker.calcResult();
			fearTracker.flush();
			if(fear != null)
				drawArticle(fear, x, y + h*.3, color(50, 50, 200, 180));

			Article joy = (Article) joyTracker.calcResult();
			joyTracker.flush();
			if(joy != null)
				drawArticle(joy, x, y + h*.8, color(100, 150, 40, 220));

			Article disgust = (Article) disgustTracker.calcResult();
			disgustTracker.flush();
			if(disgust != null && disgust != fear)
				drawArticle(disgust, x, y + h*.8, color(150, 140, 0, 220));
		}
	}

	void drawArticle(Article a, float x, float y, color col)
	{


		pushMatrix();

		translate(x, y);
		//rotate(radians(20));

		noStroke();
		fill(col);
		//ellipse(0,0,4,4);

		/*PImage image = a.getImage();
		float imgw = image.width;
		float imgh = image.height;
		float ratio = imgh/imgw;

		float imgSize = 40;
		image(image, 11, 0, imgSize, imgSize * ratio);

		textSize(14);
		textAlign(LEFT, TOP);
		fill(0, 100);
		text(a.title, 11 + imgSize + 10, 2, h*1.3, imgSize * 2);*/

		textSize(12);
		textAlign(LEFT, TOP);
		fill(col);
		text(a.title, 0, 0, h*.8, 300);


		popMatrix();
	}
}