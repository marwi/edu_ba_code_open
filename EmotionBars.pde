public class EmotionBars extends Histo
{
	boolean minified = false;

	public EmotionBars(ArrayList<Article> articles)
	{
		super(articles);
		setup();
	}

	void setup()
	{

	}

	void draw()
	{
		noFill();
		stroke(0,20);
		strokeWeight(1);
		//rect(x,y,w,h);

		ToneAvg avg = new ToneAvg();
		avg.get(articles);

		float anger = avg.anger,
		fear = avg.fear,
		sadness = avg.sadness,
		disgust = avg.disgust,
		joy = avg.joy;

		drawPlot(0, avg, "anger");
		drawPlot(1, avg, "disgust");
		drawPlot(2, avg, "sadness");
		drawPlot(3, avg, "fear");
		drawPlot(4, avg, "joy");
	}

	void drawPlot(float i, ToneAvg toneAvg, String type)
	{
		float median = 0, max = 0, min = 0, avg = 0;
		String text = "";
		PImage img = null;

		switch(type)
		{
			case "anger":
			median = toneAvg.anger;
			max = toneAvg.maxAnger;
			min = toneAvg.minAnger;
			avg = average.anger;
			text = "a";
			img = icons.anger();
			break;

			case "joy":
			median = toneAvg.joy;
			max = toneAvg.maxJoy;
			min = toneAvg.minJoy;
			avg = average.joy;
			text = "j";
			img = icons.joy();
			break;

			case "fear": 
			median = toneAvg.fear;
			max = toneAvg.maxFear;
			min = toneAvg.minFear;
			avg = average.fear;
			text = "f";
			img = icons.fear();
			break;

			case "sadness":
			median = toneAvg.sadness;
			max = toneAvg.maxSadness;
			min = toneAvg.minSadness;
			avg = average.sadness;
			text = "s";
			img = icons.sadness();
			break;

			case "disgust": 
			median = toneAvg.disgust;
			max = toneAvg.maxDisgust;
			min = toneAvg.minDisgust;
			avg = average.disgust;
			text = "d";
			img = icons.disgust();
			break;
		}

		float difference = median - avg;
		float minDifference = min - avg;
		float maxDifference = max - avg;

		float columnWidth = w / 5;
		float x = this.x + (columnWidth) * i + columnWidth/2;


		float plotCenter = y + h/2;

		// draw center line
		noFill();
		stroke(0, 80);
		strokeWeight(1);
		line(x, y, x, y+h);

		// draw plot end line
		if(!minified)
		{
			float endWidth = 12;
			endWidth /= 2;
			line(x-endWidth, y, x+endWidth, y);
			line(x-endWidth, y+h, x+endWidth, y+h);	
		}

		// draw difference
		if(abs(difference) > .1)
		{
			if(difference < 0)
				stroke(71, 120, 176);
			else
				stroke(73, 138, 99);

			noFill();
			float thickness = 4;
			strokeWeight(1);
			strokeCap(SQUARE);
			stroke(0,100);
			//rect(x, y + h/2, barWidth, barHeight * -1);
			//line(x, y + h/2, x, y + h/2 - difference * h/2 * 2);
			float barY = y + h/2 - minDifference;
			//rect(x-thickness/2, barY, thickness, barY + maxDifference * )

			fill(0);
			float centerY = y+h/2 - difference * h/2;
			stroke(0);
			strokeWeight(2);
			line(x, centerY, x, y+h/2);

			if(!minified)
			{
				strokeWeight(3);
				line(x-4, centerY, x+4, centerY);
			}
		}
		else 
		{
			stroke(120);
			strokeWeight(1);
			float centerLength = 6;

			if(minified) centerLength *= .5;

			line(x-centerLength/2, y+h/2, x+centerLength/2, y+h/2);
		}	

		if(!minified)
		{
			// little legend
			noStroke();
			fill(0, 100);
			textSize(12);
			textAlign(CENTER, TOP);
			text(text, x, y + h + 5);	
			
			if(img != null)
			{
				float imgw = 14;
				//tint(0, 100);
				//image(img, x - imgw/2, y + h + 5, imgw, imgw);
			}
			else
			{
			}
		}
	}
}