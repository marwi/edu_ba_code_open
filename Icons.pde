public interface IIcon
{
	PImage get();
}

public class Icon implements IIcon
{
	PImage icon;

	public Icon(String path)
	{
		icon = loadImage(path);
	}

	public PImage get()
	{
		return icon;
	}
}

public class EmotionIcons
{
	IIcon anger, fear, sadness, disgust, joy;

	public EmotionIcons()
	{
		String folder = "icons/";
		String prefix = "icons_";
		String ext = ".png";

		anger = new Icon(folder + prefix + "anger" + ext);
		fear = new Icon(folder + prefix + "fear" + ext);
		sadness = new Icon(folder + prefix + "sadness" + ext);
		disgust = new Icon(folder + prefix + "disgust" + ext);
		joy = new Icon(folder + prefix + "joy" + ext);
	}

	public PImage anger()
	{
		return anger.get();
	}

	public PImage fear()
	{
		return fear.get();
	}

	public PImage sadness()
	{
		return sadness.get();
	}

	public PImage disgust()
	{
		return disgust.get();
	}

	public PImage joy()
	{
		return joy.get();
	}
}