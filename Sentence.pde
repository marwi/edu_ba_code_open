// D:\Personal\Uni\2016\UrbaneEbenen\nyt_downloads\4fd2a5688eb7c8105d88cc28

public class Sentence extends ToneData
{
	boolean valid = false;
	boolean isValid() { return valid; }

	Article article;

	int id, from, to;
	String text;
	String[] subsentences;

	Sentence(Article article, JSONObject sentenceJson)
	{
		this.article =  article;

		try 
		{
			if(sentenceJson != null)
			{
				init(sentenceJson);
			}
		}
		catch(Exception e) 
		{ 	
			//println("Sentence: " + e); 
			valid = false; 
		}
	}

	void init(JSONObject sentenceJson)
	{
		id = sentenceJson.getInt("sentence_id");
		text = sentenceJson.getString("text");
		from = sentenceJson.getInt("input_from");
		to = sentenceJson.getInt("input_to");

		JSONArray toneCategories = sentenceJson.getJSONArray("tone_categories");
		readTone(toneCategories);

		String t = text;//text.replace('\n', ' ');
		text = text.replace("?", "'");
		text = text.replace(".", "");
		subsentences = text.split("(i)[, ]but | and | or |[.,!?:;\n]");
		subsentences = removeSpacesAtStartAndEnd(subsentences);

		valid = true;
	}

	String[] getWords()
	{
		String t = text.replace('\n', ' ');
		return splitTokens(t, " ");
	}

	String[] getSubsentences()
	{
		return subsentences;
	}

	String[] removeSpacesAtStartAndEnd(String[] strings)
	{
		for(int i = 0; i < strings.length; i++)
		{
			String str = strings[i];
			String[] words = splitTokens(str, " ");
			str = "";
			for(int j = 0; j < words.length; j++)
			{
				String w = words[j];
				if(w != " ")
				{
					str += w;
					if(j < words.length-1)
						str += " ";
				}
			}
			strings[i] = str;
		}
		return strings;
	}

	Author[] getAuthors()
	{
		return article.getAuthors();
	}
}

