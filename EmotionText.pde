public class EmotionText extends Histo
{
	public Date earliest, oldest;

	ArrayList<Article> dataSet;
	int year;
	String emotion;
	float emotionMinimum;

	ArrayList<Sentence> sentences;

	public EmotionText(ArrayList<Article> articles, String emotion) 
	{ 
		super(articles);
		this.emotion = emotion;
		setup();
	}

	void setup()
	{
		emotionMinimum = .2;
		year = 2013;
		dataSet = new ArrayList<Article>();
		sentences = new ArrayList<Sentence>();

		for(Article a : articles)
		{
			Date d = a.pubDate;
			int y = d.getYear();
			if(y == year && a.documentType.contains("article"))
			{
				addArticle(a);
			}
		}

		myFont = createFont("Palatino Linotype", 30);
	}

	void addArticle(Article a)
	{
		if(a.getEmotion(emotion) > emotionMinimum)
			dataSet.add(a);

		for(int i = 0; i < a.sentences.size(); i++)
		{
			Sentence s = a.sentences.get(i);
			float em = s.getEmotion(emotion);
			if(em >= emotionMinimum)
			{
				sentences.add(s);
			}
		}
	}


	PFont myFont;

	void draw()
	{
		noFill();
		stroke(0, 0);
		rect(x,y,w,h);


		Collections.sort(this.sentences, new Comparator<Sentence>(){
			public int compare(Sentence a1, Sentence a2) 
			{
				return a1.article.pubDate.totalDays() - a2.article.pubDate.totalDays(); 				
			}
		});

		Collections.sort(this.dataSet, new Comparator<Article>(){
			public int compare(Article a1, Article a2) 
			{
				float emotion1 = a1.getEmotion(emotion);
				float emotion2 = a2.getEmotion(emotion); 	
				if(emotion1 > emotion2) return -1;
				else if(emotion2 > emotion1) return 1;
				else return 0;			
			}
		});

		float px = x;
		float py = y + 150;

		for(Article article : dataSet)
		{
			String t = article.title + " (" + article.pubDate + ")\n";


			PImage img = article.getImage();
			if(img != null)
			{
				image(img, px, py - 55, 100, 100);
			}

			fill(0);  
			textFont(myFont);
			textAlign(LEFT, TOP);
			textSize(14);
			textLeading(20);
			text("" + article.getEmotion(emotion), px, py + 60, 320, 45);

			float psy = py + 45;
			float psx = px;

			for(Sentence sentence : article.sentences)
			{
				if(sentence.getEmotion(emotion) > .7)
				{
					String text = sentence.text;

					fill(0);  
					textFont(myFont);
					textAlign(LEFT, TOP);
					textSize(9);
					textLeading(12);
					//text(text, psx, psy, 320, 40);

					psy += 44;
				}
			}

			px += 105;

			if(px >= x + w)
			{
				px = x;
				py += 165;
			}
		}
		/*for(Sentence s : sentences)
		{
			t += s.text + "(" + s.article.pubDate + ") ";
		}*/


		String description =  "emotion: \"" + emotion + "\", minimum: " + emotionMinimum;
		description += "\n" + dataSet.size() + " titles, sorted by emotion up (high) to down (low), " + year;

		fill(0);
		textAlign(LEFT, TOP);
		textSize(20);
		text(description, x + 5, y);
	}
}