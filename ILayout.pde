public interface ILayouter
{
	void layout();
}

public abstract class Layouter extends GenHisto<ArrayList<IHisto>> implements ILayouter
{
	public Layouter()
	{
		super(new ArrayList<IHisto>());
	}

	public Layouter(ArrayList<IHisto> histos, float x, float y, float w, float h)
	{
		super(histos);
		setX(x);
		setY(y);
		setWidth(w);
		setHeight(h);
		setup();
	}

	void setup()
	{
		layout();
	}

	void draw()
	{
		for(IHisto histo : data)
		{
			histo.draw();
		}
	}

	void add(IHisto histo)
	{
		data.add(histo);
		layout();
	}

	abstract void layout();
}


public class ColumnsLayout extends Layouter
{
	public float lineSpacing = 0;

	public ColumnsLayout(ArrayList<IHisto> histos, float x, float y, float w, float h)
	{
		super(histos, x, y, w, h);
	}

	void layout()
	{
		LeftAligned layouter = new LeftAligned(lineSpacing);

		float columnCount = 5;
		float columnHeight = h;
		float columnSpacing = 30;
		float columnWidth = (w / columnCount) - columnSpacing;

		float columnX = x;

		ArrayList<IHisto> histos = new ArrayList<IHisto>(data);

		while(histos.size() > 0  && columnX < x + w)
		{
			histos = layouter.layout(histos, columnX, y, columnWidth, columnHeight);
			columnX += columnWidth + columnSpacing;
		}

		// remove all elements that couldn't get layouted
		if(histos.size() > 0)
		{
			for(IHisto histo : histos)
			{
				data.remove(histo);
			}
		}
	}
}