public class ImageSliceHisto extends Histo
{
	public ImageSliceHisto(ArrayList<Article> articles)
	{
		super(articles);
	}

	ArrayList<DailyImageSlice> daily;

	void setup()
	{
		daily = new ArrayList<DailyImageSlice>();

		int range = 1;

		for(int i = 0; i + range <= 365; i+=range)
		{
			int curDay = i + 1;
			IFilter day = new DayFilter(curDay, range-1);
			ArrayList<Article> filtered = (ArrayList<Article>) day.filter(articles);

			DailyImageSlice slice = new DailyImageSlice(filtered);

			float xpos = (float)i / (float)365 * w + x;
			float ypos = y;
			float cwidth = w / ((float) 365 / (float) range);

			slice.setX(xpos);
			slice.setY(ypos);
			slice.setHeight(h);
			slice.setWidth(cwidth);
			slice.setup();

			daily.add(slice);
		}
	}

	void draw()
	{
		noFill();
		strokeWeight(1);
		stroke(0, 50);
		rect(x, y, w, h);

		for(DailyImageSlice d : daily)
			d.draw();
	}
}

public class DailyImageSlice extends Histo
{
	public DailyImageSlice(ArrayList<Article> articles)
	{
		super(articles);
	}

	void setup()
	{

	}

	void draw()
	{
		for(int i = 0; i < articles.size(); i++)
		{
			Article a = articles.get(i);
			PImage img = a.getImage();

			if(img != null)
			{
				float pw = w / articles.size();
				float px = x + pw * i;
				image(img, px, y, pw, h);
			}
		}
	}
}