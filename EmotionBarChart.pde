public class EmotionBarChart extends GenHisto<Article>
{
	ArrayList<IHisto> histos;

	public EmotionBarChart(Article article, float x, float y, float w, float h)
	{
		super(article);
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		setup();
	}


		/*// colors for emotions
		drawGraph("anger", color(200, 40, 0, 100), 0);
		drawGraph("fear", color(50, 50, 200, 100), 1);
		drawGraph("disgust", color(180, 170, 0, 100), 2);
		drawGraph("sadness", color(60, 180, 220, 150), 3);
		drawGraph("joy", color(110, 160, 50, 150), 4);*/

	void setup()
	{
		histos = new ArrayList<IHisto>();

		String[] emotions = new String[]{"anger", "disgust", "fear", "sadness", "joy"};
		Integer[] colors = new Integer[]
		{
			color(200, 40, 0, 200),
			color(180, 170, 0, 200),
			color(50, 50, 200, 200),
			color(60, 180, 220, 240),
			color(110, 160, 50, 240)
		};

		float columnWidth = w / emotions.length;
		float barWidth = columnWidth * .4;
		float spacing = columnWidth - barWidth;

		for(int i = 0; i < emotions.length; i++)
		{
			String emotion = emotions[i];
			float val = data.getEmotion(emotion);
			color col = colors[i];

			float bx = x + columnWidth * i + spacing/2;
			float by = y;
			float bw = barWidth;
			float bh = h;
			Bar bar = new Bar(val, bx, by, bw, bh, col);
			histos.add(bar);
		}
	}

	void draw()
	{
		noFill();
		stroke(0, 100);
		//rect(x,y,w,h);

		/*stroke(0, 50);
		line(x, y, x+w, y);
		line(x, y+h/2, x+w, y+h/2);
		line(x, y+h, x+w, y+h);*/

		for(IHisto histo : histos)
			histo.draw();
	}
}

public class Bar extends GenHisto<Float>
{
	color col = color(200);

	public Bar(float value, float x, float y, float w, float h, Integer col)
	{
		super(value);
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.col = col;
		setup();
	}

	void setup()
	{

	}

	void draw()
	{
		noStroke();
		fill(0, 10);
		rect(x,y,w,h);
		fill(col, 30);
		rect(x,y,w,h);
		//stroke(0, 30);
		//line(x, y, x+w, y);
		//rect(x,y,w,h);

		noStroke();
		fill(col);
		rect(x,y+h - h*data, w, h*data);
	}
}