public class PoemLayout implements ILayout<ArrayList<IHisto>, Poem>
{
	public ArrayList<IHisto> layout(Poem poem, float x, float y, float w, float h)
	{
		ArrayList<IHisto> texts = new ArrayList<IHisto>();

		texts.addAll(layoutMeta(poem, x, y + 5, w, h * .2));

		ArrayList<IHisto> verses = layoutVerses(poem, x, y + h *.28, w, h *.7);
		texts.addAll(verses);


		return texts;
	}

	// title and author
	ArrayList<IHisto> layoutMeta(Poem poem, float x, float y, float w, float h)
	{
		ArrayList<IHisto> texts = new ArrayList<IHisto>();

		String title = poem.getTitle();
		println("title is " + title);
		Textbox tb = new Textbox(title, x, y + 50, w, 60);
		tb.vertical = Alignment.BOTTOM;
		tb.size = 55;
		texts.add(tb);

		Author author = poem.getAuthor();
		String name = poem.getCredits();
		Textbox au = new Textbox(name, x, tb.getY() + tb.getHeight() * 1.3, w, h * .5);
		au.vertical = Alignment.TOP;
		au.size = 30;
		texts.add(au);

		return texts;
	}

	ArrayList<IHisto> layoutVerses(Poem poem, float x, float y, float w, float h)
	{
		Strophe[] strophes = poem.getStrophes();

		ArrayList<IHisto> texts = new ArrayList<IHisto>();

		float px = x;
		float py = y;

		float defaultLineHeight = 35;
		float lineHeight = h / (poem.getVerses().length + (strophes.length)-1);
		if(lineHeight > defaultLineHeight)
			lineHeight = defaultLineHeight;

		float textSize = lineHeight*.75;

		for(Strophe strophe : strophes)
		{
			Verse[] verses = strophe.getVerses();
			for(int i = 0; i < verses.length; i++)
			{
				Verse verse = verses[i];
				String text = verse.text;

				Textbox tb = new Textbox(text, px, py, w, lineHeight);
				tb.size = (int) textSize;
				texts.add(tb);

				py += lineHeight;
			}

			py += lineHeight;
		}
		

		return texts;
	}
}