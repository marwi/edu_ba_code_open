public class ColorInfo
{
	private color c;
	private PVector pos;

	public int count = 0;

	public ColorInfo(color c, PVector pos)
	{
		this.c = c;
		this.pos = pos;
	}

	public color get()
	{
		return c;
	}

	public PVector pos()
	{
		return pos;
	}
}