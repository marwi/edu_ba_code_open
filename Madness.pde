public class Madness extends Histo
{
	ArrayList<IHisto> histos;

	public Madness(ArrayList<Article> articles, float x, float y, float w, float h)
	{
		super(articles, x, y, w, h);
	}

	void setup()
	{
		histos = new ArrayList<IHisto>();

		IHisto histo = new ImageMadness(articles, x, y, w, h);
		histos.add(histo);
	}

	void draw()
	{
		for(IHisto histo : histos)
			histo.draw();
	}
}



public class ImageMadness extends Histo
{
	public ImageMadness(ArrayList<Article> articles, float x, float y, float w, float h)
	{
		super(articles, x, y, w, h);
	}

	void setup()
	{

	}

	void draw()
	{
		for(Article a : articles)
		{
			PImage img = a.getImage();
			float rx = random(0,w);
			float ry = random(0,h);
			float size = random(40, 70);

			tint(255, 200);

			IHisto histo = new ImageBox(img, rx, ry, size, size);

			histo.draw();
		}

		tint(255);
	}
}