public class BrightnessGraph extends Graph
{
	public BrightnessGraph(ArrayList<Article> articles)
	{
		super(articles);
	}

	void setup()
	{

	}

	void drawGraph()
	{
		drawGraph("brightness", color(0, 90), 3, 1, 0);
		drawGraph("saturation", color(0, 150), 3, 1, 1);
		//drawGraph("hue", color(0, 50), 2, 1, 2);
	}

	void drawGraph(String type, color col, int lineThickness, int range, int id)
	{
		noStroke();
		fill(col);
		textAlign(RIGHT, CENTER);
		textSize(10);
		text(type, x - 50, y + id * h/3 + h/3/2);

		noFill();
		stroke(col);
		strokeWeight(lineThickness);
		strokeJoin(ROUND);

		beginShape();

		for(int i = 1; i <= 365; i += range )
		{
			DayFilter f = new DayFilter(i, range-1);
			ArrayList<Article> filtered = (ArrayList<Article>) f.filter(articles);
			float x = this.x + w * (float)i / (float)365;

			if(filtered.size() >= 1)
			{
				float totalBrightness = 0;

				for(Article a : filtered)
				{
					float articleBrightness = 0;
					for(int j = 0; j < a.paletteSize(); j++)
					{
						color acol = a.getColor(j);
						float value = 0;

						switch(type)
						{
							case "saturation":
							value = saturation(acol);
							break;

							case "brightness":
							value = brightness(acol);
							break;

							case "hue":
							value = hue(acol);
							break;
						}

						articleBrightness += value / a.paletteSize();
					}
					totalBrightness += articleBrightness;
				}

				float avgBrightness = totalBrightness / (filtered.size() * 255);

				drawVertex(x, avgBrightness);
			}
			else
			{
				drawVertex(x, 0);
			}
			
		}

		endShape();
		strokeJoin(MITER);
	}

	void drawVertex(float x, float value)
	{
		float y = this.y + h - h * value;
		vertex(x, y);
	}
}
