public abstract class MonthlyHisto extends Histo
{
	ArrayList<IHisto> histos;

	public MonthlyHisto(ArrayList<Article> articles) 
	{ 
		super(articles);
	}

	void setup()
	{
		histos = new ArrayList<IHisto>();

		int total = 0;
		for(int m = 0; m < months.length; m++)
		{
			int month = months[m];

			float hx = x + (float)total/(float)365 * w;
			float hy = y;
			float hw = (float)month/(float)365 * w;
			float hh = h;

			IFilter filter = new MonthFilter(m+1);
			ArrayList<Article> filtered = (ArrayList<Article>)filter.filter(articles);

			IHisto histo = GetHisto(filtered);
			histo.setX(hx);
			histo.setY(hy);
			histo.setWidth(hw);
			histo.setHeight(hh);
			histo.setup();

			histos.add(histo);

			total += month;
		}
	}

	void draw()
	{
		noFill();
		stroke(0,50);
		strokeWeight(1);
		//rect(x, y, w, h);

		for(int i = 0; i < histos.size(); i++)
		{
			IHisto histo = (IHisto) histos.get(i);

			if(i>0)
			{
				strokeWeight(4);
				stroke(0, 15);
				float x = histo.getX();
				float y = histo.getY();
				float h = histo.getHeight();
				line(x, y+1, x, y+h-1);
			}


			histo.draw();
		}
	}

	abstract IHisto GetHisto(ArrayList<Article> articles);
}

public class MonthlyWords extends MonthlyHisto
{
	public MonthlyWords(ArrayList<Article> articles) 
	{ 
		super(articles);
	}

	IHisto GetHisto(ArrayList<Article> articles)
	{
		return new WordHisto(articles);
	}
}

public class MonthlyEmotion extends MonthlyHisto
{
	public MonthlyEmotion(ArrayList<Article> articles) 
	{ 
		super(articles);
	}

	IHisto GetHisto(ArrayList<Article> articles)
	{
		return new EmotionGraphHisto(articles);
	}
}